# @atlaskit/reduced-ui-pack

## 8.9.2
- [patch] Fix unit tests [22337bd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/22337bd)
- [patch] Update for label with white background [a0d7ed7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a0d7ed7)
- [patch] Fix whitebackground for label [b8eb930](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b8eb930)
- [patch] Fix white background for label [229a63c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/229a63c)
- [none] Updated dependencies [22337bd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/22337bd)
  - @atlaskit/icon@11.3.1
- [none] Updated dependencies [a0d7ed7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a0d7ed7)
  - @atlaskit/icon@11.3.1
- [none] Updated dependencies [b8eb930](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b8eb930)
  - @atlaskit/icon@11.3.1
- [none] Updated dependencies [229a63c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/229a63c)
  - @atlaskit/icon@11.3.1

## 8.9.1
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/icon@11.3.0
  - @atlaskit/docs@3.0.4
  - @atlaskit/css-reset@2.0.2

## 8.9.0
- [minor] Add divider from editor  [5cbb8a6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5cbb8a6)
- [minor] Add divider fabric icon [8b794ed](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8b794ed)
- [minor] Add divider icon from fabric [c8adb64](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c8adb64)
- [none] Updated dependencies [5cbb8a6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5cbb8a6)
  - @atlaskit/icon@11.2.0
- [none] Updated dependencies [8b794ed](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8b794ed)
  - @atlaskit/icon@11.2.0
- [none] Updated dependencies [c8adb64](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c8adb64)
  - @atlaskit/icon@11.2.0

## 8.8.0
- [minor] Add label icon [72baa86](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/72baa86)

## 8.7.4
- [patch] ED-4228 adding icons for table floating toolbar advance options. [b466410](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b466410)

## 8.7.3

## 8.7.2
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 8.7.1
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 8.7.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 8.6.0
- [minor] Update svg stroke and fill color to match the spec for checkbox and radio button [aaeb66a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/aaeb66a)

## 8.5.0
- [minor] Updated switcher icon [2815441](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/2815441)

## 8.4.0
- [minor] Move icon and reduced-ui pack to new repo, update build process [b3977f3](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b3977f3)

## 8.3.0 (2017-12-08)

* feature; added new media layout icons for the editor (issues closed: ak-4012) ([ee770f5](https://bitbucket.org/atlassian/atlaskit/commits/ee770f5))
## 8.2.1 (2017-11-30)

* bug fix; update the css style to reduce padding in safari ([89cf40c](https://bitbucket.org/atlassian/atlaskit/commits/89cf40c))


## 8.2.0 (2017-11-16)

* feature; new and updated icons for the editor (issues closed: ak-3720) ([2c709e2](https://bitbucket.org/atlassian/atlaskit/commits/2c709e2))


## 8.1.0 (2017-10-10)

* feature; added 8 new icons, updated 4 others (issues closed: ak-3590) ([0cff900](https://bitbucket.org/atlassian/atlaskit/commits/0cff900))
## 8.0.0 (2017-09-25)

* breaking; Removing the "editor/expand" icon. Use the appropriate chevron-up/chevron-down icons instead. ([dc2f175](https://bitbucket.org/atlassian/atlaskit/commits/dc2f175))
* breaking; removing the "expand" icon in preference to using the chevron ones instead (issues closed: ak-2157) ([dc2f175](https://bitbucket.org/atlassian/atlaskit/commits/dc2f175))
## 7.0.0 (2017-09-11)

* breaking; The company/product icons (AtlassianIcon, BitbucketIcon, ConfluenceIcon, HipchatIcon, JiraIcon) have ([8a502b1](https://bitbucket.org/atlassian/atlaskit/commits/8a502b1))
* breaking; new company and product icons added ([8a502b1](https://bitbucket.org/atlassian/atlaskit/commits/8a502b1))
## 6.1.1 (2017-09-06)

* bug fix; reduced-ui buttons now have correct horizontal padding (issues closed: ak-3457) ([31b66b7](https://bitbucket.org/atlassian/atlaskit/commits/31b66b7))

## 6.1.0 (2017-08-28)

* feature; added switcher icon back ([de848a6](https://bitbucket.org/atlassian/atlaskit/commits/de848a6))
## 6.0.0 (2017-08-17)

* bug fix; fixing interactive coloured icon example ([8086a2c](https://bitbucket.org/atlassian/atlaskit/commits/8086a2c))
* feature; updated stories for icons and updated the build step for reduced-ui-pack icons ([0ad9eea](https://bitbucket.org/atlassian/atlaskit/commits/0ad9eea))
* breaking; syncing reduced UI pack icons with the ones from the Icon package ([d78804a](https://bitbucket.org/atlassian/atlaskit/commits/d78804a))






## 5.5.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 5.5.0 (2017-06-01)

## 5.4.0 (2017-05-31)


* fix; add prop-types as a dependency to avoid React 15.x warnings ([92598eb](https://bitbucket.org/atlassian/atlaskit/commits/92598eb))


* feature; added log-in icon to [@atlaskit](https://github.com/atlaskit)/icon and [@atlaskit](https://github.com/atlaskit)/reduced-ui-pack ([aa72586](https://bitbucket.org/atlassian/atlaskit/commits/aa72586))
* feature; update color values and usages as per #AK-2482 ([ae8fae5](https://bitbucket.org/atlassian/atlaskit/commits/ae8fae5))

## 5.3.3 (2017-05-25)


* fix; bump util-shared-styles dependency in reduced-ui-pack ([9b46b13](https://bitbucket.org/atlassian/atlaskit/commits/9b46b13))

## 5.3.2 (2017-04-27)


* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 5.3.1 (2017-04-26)

## 5.3.0 (2017-04-26)


* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))


* feature; Adds switcher icon ([220cc33](https://bitbucket.org/atlassian/atlaskit/commits/220cc33))

## 5.2.0 (2017-04-20)


* feature; removed explicit style! imports, set style-loader in webpack config ([891fc3c](https://bitbucket.org/atlassian/atlaskit/commits/891fc3c))

## 5.1.0 (2017-04-19)


* feature; add media services scale large and small icons ([3bd9d86](https://bitbucket.org/atlassian/atlaskit/commits/3bd9d86))

## 5.0.1 (2017-03-29)

## 4.0.0 (2017-03-28)

## 4.0.0 (2017-03-28)


* fix; show correct npm install command in readme story ([ccee7a2](https://bitbucket.org/atlassian/atlaskit/commits/ccee7a2))


* feature; bulk icon update ([76367b5](https://bitbucket.org/atlassian/atlaskit/commits/76367b5))
* feature; update default icon sizes ([90850bd](https://bitbucket.org/atlassian/atlaskit/commits/90850bd))


* breaking; default SVG artboard sizes are now 24px, with some icons such as editor on the 16px artboard.
* breaking; This icon released contains a large amount of breaking naming changes due to deletions and renames.

Please update to this new major version and ensure your application is using the correct icon

exports via linting.

ISSUES CLOSED: AK-1924

## 3.0.3 (2017-03-23)


* fix; Empty commit to release the component ([49c08ee](https://bitbucket.org/atlassian/atlaskit/commits/49c08ee))

## 3.0.1 (2017-03-21)

## 3.0.1 (2017-03-21)


* fix; maintainers for all the packages were added ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))

## 3.0.0 (2017-03-03)


* feature; add service desk icons ([f8d9ad4](https://bitbucket.org/atlassian/atlaskit/commits/f8d9ad4))
* feature; add reminder for breaking changes to icon affecting reduced-ui-pack ([df73682](https://bitbucket.org/atlassian/atlaskit/commits/df73682))


* breaking; Added service desk icons

## 2.5.1 (2017-02-27)


* fix; fixed the story order for reduced-ui-pack ([2d2a509](https://bitbucket.org/atlassian/atlaskit/commits/2d2a509))

## 2.5.0 (2017-02-24)


* fix; Adjusting field width for reduced-ui-pack large fields ([292dac6](https://bitbucket.org/atlassian/atlaskit/commits/292dac6))


* feature; New styles for form elements for the reduced-ui-pack ([07e6bab](https://bitbucket.org/atlassian/atlaskit/commits/07e6bab))
* feature; Updated some field colors to match the latest specs ([1448ab8](https://bitbucket.org/atlassian/atlaskit/commits/1448ab8))

## 2.4.0 (2017-02-24)


* feature; add tooltips feature to reduced UI pack ([282629d](https://bitbucket.org/atlassian/atlaskit/commits/282629d))

## 2.3.0 (2017-02-23)


* feature; add toggle component to reduced UI pack ([8eeb39e](https://bitbucket.org/atlassian/atlaskit/commits/8eeb39e))

## 2.2.0 (2017-02-21)


* fix; change directory icon specific names to generic icon names ([13bb38a](https://bitbucket.org/atlassian/atlaskit/commits/13bb38a))


* feature; directory icons ([b278823](https://bitbucket.org/atlassian/atlaskit/commits/b278823))

## 2.1.0 (2017-02-20)


* feature; add icons to reduced-ui-pack as svg name export ([c05ba4f](https://bitbucket.org/atlassian/atlaskit/commits/c05ba4f))

## 2.0.1 (2017-02-14)


* fix; Rewording docs and triggering a release. ([27c430f](https://bitbucket.org/atlassian/atlaskit/commits/27c430f))

## 1.0.0 (2017-02-10)


* fix; Updates from pull request feedback ([93d90c7](https://bitbucket.org/atlassian/atlaskit/commits/93d90c7))


* feature; Grid and Button patterns for the reduced-ui-pack ([8336747](https://bitbucket.org/atlassian/atlaskit/commits/8336747))
