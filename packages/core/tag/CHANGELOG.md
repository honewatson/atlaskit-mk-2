# @atlaskit/tag

## 4.1.1
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/icon@11.3.0
  - @atlaskit/theme@3.2.2
  - @atlaskit/avatar@10.0.6
  - @atlaskit/docs@3.0.4

## 4.1.0
- [minor] Add linkComponent to allow passing of custom link components [d2d2678](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d2d2678)

## 4.0.2

## 4.0.1
- [patch] Remove unused dependencies [3cfb3fe](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3cfb3fe)

## 4.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 3.2.1
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 3.2.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 3.1.3
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 3.1.2
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 3.1.1

## 3.1.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 3.0.14

## 3.0.13

## 3.0.12




- [patch] Fixed flow error [c3d78ba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c3d78ba)
- [patch] Fixed flow error [c3d78ba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c3d78ba)

## 3.0.11

## 3.0.10
- [patch] Fix references to link and linkHover (in theme)  from the Tag styles components [d509c86](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d509c86)
- [patch] Fix references to link and linkHover (in theme)  from the Tag styles components [d509c86](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d509c86)

## 3.0.9

## 3.0.8
- [patch] bump icon dependency [da14956](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/da14956)
- [patch] bump icon dependency [da14956](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/da14956)

## 3.0.7

## 3.0.6
- [patch] Use correct dependencies  [7b178b1](7b178b1)
- [patch] Use correct dependencies  [7b178b1](7b178b1)
- [patch] Adding responsive behavior to the editor. [e0d9867](e0d9867)
- [patch] Adding responsive behavior to the editor. [e0d9867](e0d9867)

## 3.0.2 (2017-09-20)

* bug fix; fix text colors, styling for colored tags (issues closed: #ak-3557) ([8c48e98](https://bitbucket.org/atlassian/atlaskit/commits/8c48e98))

## 3.0.1 (2017-08-30)

* bug fix; change darkmode colors after spec update to 1.3 ([5d78178](https://bitbucket.org/atlassian/atlaskit/commits/5d78178))

## 3.0.0 (2017-08-23)

* breaking; Tag has dark mode colors ([149b8df](https://bitbucket.org/atlassian/atlaskit/commits/149b8df))
* breaking; tag has dark mode colors (issues closed: #ak-3239) ([149b8df](https://bitbucket.org/atlassian/atlaskit/commits/149b8df))
* Adding Dark mode, so that when the atlaskit theme manager is used it can be turned on. This should not be a breaking any old functionality, but as it is a major rewrite, it is a major version bump.

## 2.6.0 (2017-08-03)

* feature; add color options for tag (issues closed: #ak-2910) ([76831b4](https://bitbucket.org/atlassian/atlaskit/commits/76831b4))

## 2.5.2 (2017-07-27)

* fix; rename jsnext:main to jsnext:experimental:main temporarily ([c7508e0](https://bitbucket.org/atlassian/atlaskit/commits/c7508e0))

## 2.5.1 (2017-07-25)

* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 2.2.0 (2017-07-17)

## 2.2.0 (2017-07-17)

## 2.2.0 (2017-07-17)

* fix; rerelease, failed prepublish scripts ([5fd82f8](https://bitbucket.org/atlassian/atlaskit/commits/5fd82f8))

## 2.2.0 (2017-07-17)

* feature; added ES module builds to dist and add jsnext:main to most ADG packages ([ea76507](https://bitbucket.org/atlassian/atlaskit/commits/ea76507))

## 2.1.4 (2017-06-02)

* fix; Peerdependency is now [@atlaskit](https://github.com/atlaskit)/tag not ak-tag ([418e0b3](https://bitbucket.org/atlassian/atlaskit/commits/418e0b3))
* fix; add prop-types as a dependency to avoid React 15.x warnings ([92598eb](https://bitbucket.org/atlassian/atlaskit/commits/92598eb))

## 2.1.3 (2017-05-03)

* fix; fixes a problem where Tag tries to import a styled component from util-shared-styles@^ ([c58d643](https://bitbucket.org/atlassian/atlaskit/commits/c58d643))

## 2.1.2 (2017-04-27)

* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 2.1.1 (2017-04-26)

* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))

## 2.1.0 (2017-04-20)

* feature; removed explicit style! imports, set style-loader in webpack config ([891fc3c](https://bitbucket.org/atlassian/atlaskit/commits/891fc3c))

## 2.0.0 (2017-04-05)

null refactor the tag component to use styled-components ([7a7a513](https://bitbucket.org/atlassian/atlaskit/commits/7a7a513))

* breaking; added peerDependency "styled-components", removed dependency "classnames"

ISSUES CLOSED: AK-2033

## 1.2.1 (2017-04-02)

* fix; change one of the maintainers ([e9a3011](https://bitbucket.org/atlassian/atlaskit/commits/e9a3011))

## 1.2.0 (2017-03-28)

* fix; fIxes bug in firefox where outline was shown for rounded tag buttons ([0936fd5](https://bitbucket.org/atlassian/atlaskit/commits/0936fd5))
* feature; adds appearance prop to tags to allow "rounded" appearance ([fa1df38](https://bitbucket.org/atlassian/atlaskit/commits/fa1df38))

## 1.1.3 (2017-03-23)

* fix; Empty commit to release the component ([49c08ee](https://bitbucket.org/atlassian/atlaskit/commits/49c08ee))

## 1.1.1 (2017-03-21)

## 1.1.1 (2017-03-21)

* fix; maintainers for all the packages were added ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))

## 1.1.0 (2017-03-07)

* feature; adds elemBefore prop to tags ([f0faae4](https://bitbucket.org/atlassian/atlaskit/commits/f0faae4))

## 1.0.5 (2017-02-28)

* fix; removes jsdoc annoations and moves content to usage.md ([d133f5d](https://bitbucket.org/atlassian/atlaskit/commits/d133f5d))
* fix; dummy commit to release stories ([3df5d9f](https://bitbucket.org/atlassian/atlaskit/commits/3df5d9f))

## 1.0.4 (2017-02-10)

## 1.0.4 (2017-02-10)

* fix; Updates package to have correct dev-dependency for util-common-test ([403d232](https://bitbucket.org/atlassian/atlaskit/commits/403d232))

## 1.0.3 (2017-02-09)

* fix; avoiding binding render to this ([40c9951](https://bitbucket.org/atlassian/atlaskit/commits/40c9951))

## 1.0.2 (2017-02-07)

## 1.0.1 (2017-02-07)

* fix; Updates package to use scoped ak packages ([b4dd274](https://bitbucket.org/atlassian/atlaskit/commits/b4dd274))
* fix; Fixes incorrect import ([3612a97](https://bitbucket.org/atlassian/atlaskit/commits/3612a97))
* fix; prevent dismiss button from submitting the parent form ([32ddf63](https://bitbucket.org/atlassian/atlaskit/commits/32ddf63))
