# @atlaskit/theme

## 3.2.2
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/button@7.2.5
  - @atlaskit/docs@3.0.4

## 3.2.1
- [patch] Add Consolas to our font family [62bacf6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/62bacf6)
- [none] Updated dependencies [62bacf6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/62bacf6)

## 3.2.0
- [minor] Add color palette to theme - Jira Porfolio [72ab054](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/72ab054)

## 3.1.1
- [patch] releasing all compo that depends on theme [86a82d2](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/86a82d2)
- [patch] Re-release to fix changes merged with @atlaskit/form [baa3c20](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/baa3c20)

## 3.1.0
- [minor] Added ability to specify an object as the badge appearance. Added an Appearance export to theme so that we can use strings and objects for appearance theming." [6e89615](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6e89615)

## 3.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 2.4.1
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 2.4.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 2.3.4
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 2.3.3
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 2.3.2
- [patch] added a new layer tooltip [2215bc7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/2215bc7)

## 2.3.1
- [patch] Resolved low hanging flow errors in field-base field-text comment icon item and website, $ [007de27](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/007de27)

## 2.3.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 2.2.9

## 2.2.8

## 2.2.7

## 2.2.6

## 2.2.5

## 2.2.4
- [patch] moved theme to new atlaskit repo [a25b940](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a25b940)
- [patch] moved theme to new atlaskit repo [a25b940](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a25b940)

## 2.2.3 (2017-10-27)

* bug fix; triggering storybooks ([87e7247](https://bitbucket.org/atlassian/atlaskit/commits/87e7247))
* bug fix; removed unused dependency on util-shared-styles from the Theme component ([253d8fc](https://bitbucket.org/atlassian/atlaskit/commits/253d8fc))

## 2.2.2 (2017-10-26)

* bug fix; fix to rebuild stories ([793b2a7](https://bitbucket.org/atlassian/atlaskit/commits/793b2a7))

## 2.2.1 (2017-10-22)

* bug fix; update styled components dep and react peerDep ([5539ada](https://bitbucket.org/atlassian/atlaskit/commits/5539ada))

## 2.2.0 (2017-09-27)

* feature; export "layers" from theme ([15aebe6](https://bitbucket.org/atlassian/atlaskit/commits/15aebe6))

## 2.1.0 (2017-09-13)

* feature; [@atlaskit](https://github.com/atlaskit)/theme now has a named getTheme() function export ([b727679](https://bitbucket.org/atlassian/atlaskit/commits/b727679))

## 2.0.1 (2017-08-11)

* bug fix; Add placeholder color to theme ([ba023fb](https://bitbucket.org/atlassian/atlaskit/commits/ba023fb))

## 2.0.0

Initial Release
