# @atlaskit/table-tree

## 2.0.1
- [patch] Remove line break from changelog to allow it to display properly on the website [9e30bb1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9e30bb1)

## 2.0.0
- [major] updated the api to capture scenarios where data can be updated on the fly [c1720e8](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c1720e8)
- updated the `items` prop in TableTree component to accept Array of table data instead of function
- updated the `items` prop in Rows component to accept Array of table data instead of function
- added an `items` prop in Row component to accept children data Array for particular parent
- a new class is exported that will help manipulation for async loading `TableTreeDataHelper`, this is intended to make upgrade from previous API easy in case of async loading.

## 1.1.4
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/icon@11.3.0
  - @atlaskit/select@3.0.2
  - @atlaskit/button@7.2.5
  - @atlaskit/theme@3.2.2
  - @atlaskit/spinner@5.0.2
  - @atlaskit/docs@3.0.4

## 1.1.3
- [patch] Updated dependencies [d05b9e5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d05b9e5)
  - @atlaskit/select@3.0.0

## 1.1.2

## 1.1.1

## 1.1.0
- [minor] Improve accessibility. Use AkButton for the Chevrons. [8ec5a94](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8ec5a94)

## 1.0.1
- [patch] Update deps and examples. [b775a12](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b775a12)
- [patch] Add a performance example [8eb1472](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8eb1472)

## 1.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 0.6.1
- [patch] Makes packages Flow types compatible with version 0.67 [25daac0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/25daac0)

## 0.6.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 0.5.3
- [patch] Fix an indirect race condition vulnerability [b75c02a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b75c02a)

## 0.5.2
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 0.5.1
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 0.5.0


- [minor] Add accessibility features; introduce Row's itemId prop [2e0807f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/2e0807f)

## 0.4.0
- [minor] Update header design to conform to ADG3 [6170e98](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6170e98)
- [minor] Don't display the loader if data is available quickly [93fd2eb](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/93fd2eb)

## 0.3.2
- [patch] Fix: ellipsis was not shown when overflowing text was clipped [05034f7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/05034f7)

## 0.3.1
- [patch] Fix setState being called after unmount [4e5ca03](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4e5ca03)

## 0.3.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 0.2.2
- [patch] Styling/spacing adjustments [0c29170](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0c29170)

## 0.2.1
- [patch] Fix Table Tree readme to point to the correct screencast [ba4a01f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ba4a01f)

## 0.2.0
- [minor] Add the Table Tree component (alpha version) [53ec386](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/53ec386)
