# @atlaskit/spinner

## 5.0.2
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/button@7.2.5
  - @atlaskit/theme@3.2.2
  - @atlaskit/docs@3.0.4

## 5.0.1

## 5.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 4.2.1
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 4.2.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 4.1.5
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 4.1.4
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 4.1.3
- [patch] Flatten examples for easier consumer use [145b632](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/145b632)

## 4.1.2
- [patch] Fix spinner performance by statically defining keyframes [6f04599](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6f04599)

## 4.1.1
- [patch] Resolved low hanging flow errors in field-base field-text comment icon item and website, $ [007de27](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/007de27)

## 4.1.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 4.0.8

## 4.0.7

## 4.0.6
- [patch] fix Spinner's onComplete prop not being called [91e8994](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/91e8994)
- [patch] fix Spinner's onComplete prop not being called [91e8994](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/91e8994)

## 4.0.5

## 4.0.4

## 4.0.3

## 4.0.2 (2017-10-26)

* bug fix; fix to rebuild stories ([793b2a7](https://bitbucket.org/atlassian/atlaskit/commits/793b2a7))

## 4.0.1 (2017-10-22)

* bug fix; update styled-components dep and react peerDep ([6a67bf8](https://bitbucket.org/atlassian/atlaskit/commits/6a67bf8))
## 4.0.0 (2017-08-29)



* breaking; convert spinner to have a dark mode implementation ([b1c2a53](https://bitbucket.org/atlassian/atlaskit/commits/b1c2a53))
* breaking; convert spinner to have a dark mode implementation (issues closed: #ak-3371) ([b1c2a53](https://bitbucket.org/atlassian/atlaskit/commits/b1c2a53))






## 3.4.2 (2017-07-27)


* fix; rename jsnext:main to jsnext:experimental:main temporarily ([c7508e0](https://bitbucket.org/atlassian/atlaskit/commits/c7508e0))

## 3.4.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 3.1.0 (2017-07-17)

## 3.1.0 (2017-07-17)

## 3.1.0 (2017-07-17)


* fix; rerelease, failed prepublish scripts ([5fd82f8](https://bitbucket.org/atlassian/atlaskit/commits/5fd82f8))

## 3.1.0 (2017-07-17)


* feature; added ES module builds to dist and add jsnext:main to most ADG packages ([ea76507](https://bitbucket.org/atlassian/atlaskit/commits/ea76507))

## 3.0.0 (2017-07-07)


* fix; refactor spinner to use TransitionGroup ([e0cef25](https://bitbucket.org/atlassian/atlaskit/commits/e0cef25))


* breaking; remove typescript interface file. when a spinner's isCompleting props is true and it has finished
its exit animation it will not longer take up space in the DOM.

ISSUES CLOSED: AK-2559

## 2.2.5 (2017-06-28)


* fix; triggering component release with previous fix ([20a9e93](https://bitbucket.org/atlassian/atlaskit/commits/20a9e93))

## 2.2.4 (2017-06-27)


* fix; fix Spinner onComplete invocation on Edge ([e998791](https://bitbucket.org/atlassian/atlaskit/commits/e998791))

## 2.2.3 (2017-05-29)


* fix; add index shim for unit tests ([838c743](https://bitbucket.org/atlassian/atlaskit/commits/838c743))
* fix; add prop-types as a dependency to avoid React 15.x warnings ([92598eb](https://bitbucket.org/atlassian/atlaskit/commits/92598eb))

## 2.2.2 (2017-05-25)


* fix; update util-shared-styles dependency in spinner ([603a1c1](https://bitbucket.org/atlassian/atlaskit/commits/603a1c1))

## 2.2.1 (2017-05-08)


* fix; removes spinner in button story from storybook ([96ed0f8](https://bitbucket.org/atlassian/atlaskit/commits/96ed0f8))

## 2.2.0 (2017-05-06)


* feature; add invertColor prop to Spinner to enable it to be used on dark backgrounds ([4981617](https://bitbucket.org/atlassian/atlaskit/commits/4981617))

## 2.1.0 (2017-05-02)


* feature; adds delay prop to Spinner for custom delays before showing spinner ([98679b7](https://bitbucket.org/atlassian/atlaskit/commits/98679b7))
* feature; fixes issue where long delays would remove the spin-in animation ([240bea9](https://bitbucket.org/atlassian/atlaskit/commits/240bea9))

## 2.0.3 (2017-04-27)


* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 2.0.2 (2017-04-26)


* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))

## 2.0.1 (2017-03-28)


* fix; remove [@atlaskit](https://github.com/atlaskit)/spinner from same package devDependencies ([2eac9f0](https://bitbucket.org/atlassian/atlaskit/commits/2eac9f0))

## 2.0.0 (2017-03-27)


* fix; address IE focus management issue ([acad36d](https://bitbucket.org/atlassian/atlaskit/commits/acad36d))


null refactor the spinner component to use styled-components ([155956c](https://bitbucket.org/atlassian/atlaskit/commits/155956c))


* breaking; removed dependency \ as dependency \| added dependency \ \|

added peerDependency of \

## 1.0.5 (2017-03-22)


* fix; remove spinner delay when removing component ([5c2ebcf](https://bitbucket.org/atlassian/atlaskit/commits/5c2ebcf))

## 1.0.3 (2017-03-21)

## 1.0.3 (2017-03-21)


* fix; maintainers for all the packages were added ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))

## 1.0.2 (2017-03-03)


* fix; adds a 100ms delay before showing the spinner ([90d9a47](https://bitbucket.org/atlassian/atlaskit/commits/90d9a47))
* fix; minor docs/storybook updates to be more in line with the rest of the Atlaskit patterns ([83a0af1](https://bitbucket.org/atlassian/atlaskit/commits/83a0af1))

## 1.0.1 (2017-02-07)


* fix; Updates package to use scoped ak packages ([aa32414](https://bitbucket.org/atlassian/atlaskit/commits/aa32414))
