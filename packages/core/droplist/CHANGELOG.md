# @atlaskit/droplist

## 5.0.3
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/icon@11.3.0
  - @atlaskit/tooltip@9.2.1
  - @atlaskit/item@6.0.3
  - @atlaskit/field-base@9.0.3
  - @atlaskit/button@7.2.5
  - @atlaskit/theme@3.2.2
  - @atlaskit/spinner@5.0.2
  - @atlaskit/docs@3.0.4
  - @atlaskit/layer@3.1.1

## 5.0.2

## 5.0.1

- [patch] Fix clipping of dropdown item content due to line height issues [a0392ec](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a0392ec)

## 5.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 4.12.2
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 4.12.1
- [patch] Update links in documentation [c4f7497](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c4f7497)

## 4.12.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 4.11.11
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 4.11.10
- [patch] fixes AK-4178 , added fix for double color icon in navigation [c6121d6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c6121d6)

## 4.11.9

## 4.11.8
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 4.11.7
- [patch] Change incorrect type info [ce915ea](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ce915ea)

## 4.11.6
- [patch] transparent background for selected item in droplist [75445a6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/75445a6)

## 4.11.5
- [patch] migrate from ak to mk2 -> droplist [3b0ae0c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3b0ae0c)

## 4.11.4 (2017-12-04)

* bug fix; updated backgorund and color for droplist item in selected mode (issues closed: ak-3624) ([c1cb987](https://bitbucket.org/atlassian/atlaskit/commits/c1cb987))

## 4.11.3 (2017-11-15)

* bug fix; bumping to latest major version of internal dependencies ([be44c19](https://bitbucket.org/atlassian/atlaskit/commits/be44c19))
## 4.11.2 (2017-10-22)

* bug fix; update dependencies for react-16 ([077d1ad](https://bitbucket.org/atlassian/atlaskit/commits/077d1ad))


## 4.11.1 (2017-09-21)

* bug fix; update item dependency (issues closed: ak-3418) ([4f64804](https://bitbucket.org/atlassian/atlaskit/commits/4f64804))






## 4.11.0 (2017-08-31)

* bug fix; droplist dependencies bumped to latest (issues closed: ak-3392) ([f24b3ba](https://bitbucket.org/atlassian/atlaskit/commits/f24b3ba))
* feature; adding the ability to pass a boundariesElement to the Layer component (issues closed: ak-3416) ([f6a215e](https://bitbucket.org/atlassian/atlaskit/commits/f6a215e))





## 4.10.0 (2017-08-29)

* bug fix; remove unused theme file ([5481b6b](https://bitbucket.org/atlassian/atlaskit/commits/5481b6b))
* feature; droplist has darkmode style options (issues closed: ak-3399) ([f4e1c65](https://bitbucket.org/atlassian/atlaskit/commits/f4e1c65))
## 4.9.0 (2017-08-11)

* bug fix; fix the theme-dependency ([db90333](https://bitbucket.org/atlassian/atlaskit/commits/db90333))

* feature; implement darkmode for droplist ([35f9281](https://bitbucket.org/atlassian/atlaskit/commits/35f9281))





## 4.8.0 (2017-08-11)

* feature; implement darkmode for droplist ([35f9281](https://bitbucket.org/atlassian/atlaskit/commits/35f9281))






## 4.7.0 (2017-07-28)

## 4.6.2 (2017-07-27)


* fix; rename jsnext:main to jsnext:experimental:main temporarily ([c7508e0](https://bitbucket.org/atlassian/atlaskit/commits/c7508e0))


* feature; convert dropdown-menu and droplist to declarative API ([f6e0292](https://bitbucket.org/atlassian/atlaskit/commits/f6e0292))

## 4.6.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 4.6.0 (2017-07-19)

## 4.2.0 (2017-07-17)

## 4.2.0 (2017-07-17)

## 4.2.0 (2017-07-17)


* fix; rerelease, failed prepublish scripts ([5fd82f8](https://bitbucket.org/atlassian/atlaskit/commits/5fd82f8))

## 4.2.0 (2017-07-17)


* feature; adds maxHeight prop to droplist ([9a25a5a](https://bitbucket.org/atlassian/atlaskit/commits/9a25a5a))
* feature; added ES module builds to dist and add jsnext:main to most ADG packages ([ea76507](https://bitbucket.org/atlassian/atlaskit/commits/ea76507))

## 4.1.0 (2017-06-21)


* feature; adds appearance prop to droplist (allowing appearance="primary") ([6c07808](https://bitbucket.org/atlassian/atlaskit/commits/6c07808))

## 4.0.1 (2017-05-31)


* fix; remove tabIndex attribute from the item with 'option' type ([a09821d](https://bitbucket.org/atlassian/atlaskit/commits/a09821d))

## 4.0.0 (2017-05-29)


null replace LESS with SC ([4532bf8](https://bitbucket.org/atlassian/atlaskit/commits/4532bf8))


* breaking; removed TypeScript

ISSUES CLOSED: AK-1517

## 3.9.0 (2017-05-26)


* fix; add prop-types as a dependency to avoid React 15.x warnings ([92598eb](https://bitbucket.org/atlassian/atlaskit/commits/92598eb))


* feature; add isLoading to Droplist ([4de3242](https://bitbucket.org/atlassian/atlaskit/commits/4de3242))

## 3.8.1 (2017-05-16)


* fix; fix type definition for droplist published in npm ([ebb882c](https://bitbucket.org/atlassian/atlaskit/commits/ebb882c))

## 3.8.0 (2017-05-10)

## 3.7.0 (2017-05-10)


* feature; add tooltip support to droplist items. ([2a2debc](https://bitbucket.org/atlassian/atlaskit/commits/2a2debc))
* feature; bumping icon in droplist component to 6.5.2 ([e99d87a](https://bitbucket.org/atlassian/atlaskit/commits/e99d87a))

## 3.6.0 (2017-05-01)


* feature; fix overflown items + additional property `shouldAllowMultilineItems` ([6af80cc](https://bitbucket.org/atlassian/atlaskit/commits/6af80cc))

## 3.5.1 (2017-04-20)


* fix; unmounting a droplist no longer leaves an event listener bound to the document ([6d3f7ef](https://bitbucket.org/atlassian/atlaskit/commits/6d3f7ef))

## 3.5.0 (2017-04-20)


* feature; removed explicit style! imports, set style-loader in webpack config ([891fc3c](https://bitbucket.org/atlassian/atlaskit/commits/891fc3c))

## 3.4.1 (2017-04-18)


* fix; fix problem with text descenders being cut off ([6b8e621](https://bitbucket.org/atlassian/atlaskit/commits/6b8e621))

## 3.4.0 (2017-04-18)

## 3.3.2 (2017-04-18)


* fix; bumps Layer version in droplist to allow fix from Layer ([2a1b3d2](https://bitbucket.org/atlassian/atlaskit/commits/2a1b3d2))


* feature; updated avatar dependency versions for comment, dropdown-menu, droplist, and page ([e4d2ae7](https://bitbucket.org/atlassian/atlaskit/commits/e4d2ae7))

## 3.3.1 (2017-04-06)


* fix; only remove maxHeight style for tall appearance ([76ef554](https://bitbucket.org/atlassian/atlaskit/commits/76ef554))

## 3.3.0 (2017-04-05)


* feature; avoid setting max-height when not required ([1cc2893](https://bitbucket.org/atlassian/atlaskit/commits/1cc2893))

## 3.2.0 (2017-03-29)


* feature; description in items is implemented ([73b19b1](https://bitbucket.org/atlassian/atlaskit/commits/73b19b1))

## 3.1.0 (2017-03-23)

## 3.0.3 (2017-03-21)

## 3.0.3 (2017-03-21)


* fix; maintainers for all the packages were added ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))


* feature; title property for the item sub-component is added. ([c508706](https://bitbucket.org/atlassian/atlaskit/commits/c508706))

## 3.0.2 (2017-03-20)


* fix; fix some colors and paddings according to ADG3 specs ([bf03ac9](https://bitbucket.org/atlassian/atlaskit/commits/bf03ac9))

## 3.0.1 (2017-03-14)


* fix; fix colors and some paddings to be them more adg3 ([c51eb40](https://bitbucket.org/atlassian/atlaskit/commits/c51eb40))

## 3.0.0 (2017-03-07)


* fix; fix ts definition ([0f9969a](https://bitbucket.org/atlassian/atlaskit/commits/0f9969a))
* fix; fix ts definitions ([94d3efc](https://bitbucket.org/atlassian/atlaskit/commits/94d3efc))


* feature; clean up droplist component from all keyboard interactions and trigger ([22b2034](https://bitbucket.org/atlassian/atlaskit/commits/22b2034))


* breaking; Trigger is removed from export \| isKeyboardInteractionDisabled property is removed \|

isTriggerDisabled property is removed \| isTriggerNotTabbable property is removed \| listContext

property is removed \| all keyboard functionality is removed \| all focus functionality is removed

ISSUES CLOSED: AK-1714

## 2.0.0 (2017-03-03)


* feature; merge droplist's subcomponents into the main package ([104f740](https://bitbucket.org/atlassian/atlaskit/commits/104f740))


* breaking; droplist-item, droplist-trigger, droplist-group components are now deprecated. Droplist exports them

as a named export.

## 1.3.11 (2017-02-28)


* fix; dummy commit to release stories ([3df5d9f](https://bitbucket.org/atlassian/atlaskit/commits/3df5d9f))

## 1.3.9 (2017-02-28)


* fix; dummy commit to fix broken stories and missing registry pages ([a31e92a](https://bitbucket.org/atlassian/atlaskit/commits/a31e92a))

## 1.3.9 (2017-02-28)


* fix; dummy commit to release stories for components ([a105c02](https://bitbucket.org/atlassian/atlaskit/commits/a105c02))

## 1.3.8 (2017-02-28)


* fix; Removes jsdoc annotation from droplist ([a575f49](https://bitbucket.org/atlassian/atlaskit/commits/a575f49))

## 1.3.7 (2017-02-27)


* empty commit to make components release themselves ([5511fbe](https://bitbucket.org/atlassian/atlaskit/commits/5511fbe))

## 1.3.6 (2017-02-24)


* fix the 'dummy search' behavior when dropdown is open ([4176ab3](https://bitbucket.org/atlassian/atlaskit/commits/4176ab3))

## 1.3.5 (2017-02-16)

## 1.3.4 (2017-02-15)


* fix situation when there is no items ([d249ae6](https://bitbucket.org/atlassian/atlaskit/commits/d249ae6))
* height of the dropdown list is calculated correctly now ([baaedc7](https://bitbucket.org/atlassian/atlaskit/commits/baaedc7))

## 1.3.2 (2017-02-14)

## 1.3.2 (2017-02-14)


* fix broken focus ring and scrolling ([6e30737](https://bitbucket.org/atlassian/atlaskit/commits/6e30737))

## 1.3.1 (2017-02-10)


* fix; fix broken focus ring ([1959f0f](https://bitbucket.org/atlassian/atlaskit/commits/1959f0f))
* fix; refactor stories to use // rather than http:// ([a0826cf](https://bitbucket.org/atlassian/atlaskit/commits/a0826cf))

## 1.3.0 (2017-02-10)

## 1.2.1 (2017-02-09)


* fix; avoiding binding render to this ([40c9951](https://bitbucket.org/atlassian/atlaskit/commits/40c9951))


* feature; isKeyboardInteractionDisabled prop is added ([2b1ec2c](https://bitbucket.org/atlassian/atlaskit/commits/2b1ec2c))
