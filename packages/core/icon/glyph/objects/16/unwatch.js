'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects16UnwatchIcon = function Objects16UnwatchIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M6 4h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2zm0 8c0 1.929 2.747 5 6 5 3.254 0 6-3.071 6-5 0-1.975-2.688-5-6-5s-6 3.025-6 5zm2 0c0-.363.41-1.12 1.115-1.771C9.952 9.455 10.992 9 12 9c1.009 0 2.048.455 2.885 1.229C15.59 10.88 16 11.637 16 12c0 .33-.42 1.093-1.136 1.754C14.014 14.538 12.975 15 12 15c-.975 0-2.014-.462-2.864-1.247C8.42 13.093 8 12.331 8 12zm4 1.5a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects16UnwatchIcon;