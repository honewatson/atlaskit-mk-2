'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects16SupportIcon = function Objects16SupportIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M4 5.994C4 4.893 4.895 4 5.994 4h12.012C19.107 4 20 4.895 20 5.994v12.012A1.995 1.995 0 0 1 18.006 20H5.994A1.995 1.995 0 0 1 4 18.006V5.994zm12.983 4.977c.067-.803-.062-1.56-.483-2.245-.537-.922-1.569-1.39-2.578-1.17a2.916 2.916 0 0 0-1.58.916c-.12.127-.228.267-.34.4a.079.079 0 0 1-.025-.01l-.077-.098a3.372 3.372 0 0 0-1.128-.965c-.982-.51-2.085-.364-2.861.407C7.252 8.86 7 9.693 7 10.627c.008.772.227 1.526.633 2.17.552.912 1.23 1.733 2.014 2.434.452.426.962.779 1.512 1.048.32.148.645.256 1 .21.458-.06.86-.27 1.241-.524 1.114-.746 2.015-1.722 2.77-2.851.43-.646.746-1.343.813-2.143z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects16SupportIcon;