'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects16CommentIcon = function Objects16CommentIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M6 4h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2zm10.354 12.166s-.868-1.296-.426-1.786C16.59 13.656 17 12.726 17 11.715c0-2.374-2.243-4.305-5-4.305s-5 1.931-5 4.305c0 2.373 2.243 4.304 5 4.304a5.62 5.62 0 0 0 2.198-.445c.557.586 1.271.901 1.8.985.03.008.06.018.093.018a.29.29 0 0 0 .285-.296.291.291 0 0 0-.022-.115zm-1.903-3.134c.36-.394.549-.854.549-1.317 0-1.197-1.287-2.305-3-2.305s-3 1.108-3 2.305c0 1.196 1.287 2.304 3 2.304.497 0 .977-.1 1.418-.286l.738-.313c.082-.131.18-.261.295-.388z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects16CommentIcon;