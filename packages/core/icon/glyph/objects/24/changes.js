'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects24ChangesIcon = function Objects24ChangesIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M16.587 15H5a1 1 0 0 0 0 2h11.591l-1.298 1.296a1.001 1.001 0 0 0 1.414 1.416l3.005-3.001a1.002 1.002 0 0 0 0-1.415l-3.005-3.003a.999.999 0 1 0-1.414 1.414L16.587 15zM7.418 7l1.294-1.293a.999.999 0 1 0-1.414-1.414L4.293 7.296a1 1 0 0 0 0 1.415l3.005 3a1 1 0 0 0 1.414-1.415L7.414 9H19a1 1 0 0 0 0-2H7.418zM3 0h18a3 3 0 0 1 3 3v18a3 3 0 0 1-3 3H3a3 3 0 0 1-3-3V3a3 3 0 0 1 3-3z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects24ChangesIcon;