'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MediaServicesZoomInIcon = function MediaServicesZoomInIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M14.5 11.5h-2v2a1 1 0 0 1-2 0v-2h-2a1 1 0 0 1 0-2h2v-2a1 1 0 0 1 2 0v2h2a1 1 0 0 1 0 2m5.208 6.778l-3.407-3.407A6.47 6.47 0 0 0 18 10.5a6.5 6.5 0 1 0-6.5 6.5c1.181 0 2.286-.32 3.24-.871.008.009.01.02.019.028l3.535 3.536a1.003 1.003 0 0 0 1.414 0 1.004 1.004 0 0 0 0-1.415" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = MediaServicesZoomInIcon;