'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MediaServicesActualSizeIcon = function MediaServicesActualSizeIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><g fill="currentColor" fill-rule="evenodd"><path d="M3 15.992c0-.547.323-.668.712-.279l4.576 4.577c.393.393.261.711-.279.711H3.991a.996.996 0 0 1-.991-.99v-4.019z"/><rect x="7.001" y="8.001" width="2" height="8" rx="1"/><rect x="15" y="8.001" width="2" height="8" rx="1"/><rect x="11" y="9.001" width="2" height="2" rx="1"/><rect x="11" y="13.001" width="2" height="2" rx="1"/><path d="M3.998 3A.997.997 0 0 0 3 3.998v4.484c0 .553.316.682.705.293l5.066-5.069C9.165 3.313 9.03 3 8.48 3H3.998zm11.991.001c-.547 0-.668.323-.279.712l4.577 4.577c.393.393.712.261.712-.279V3.992a.996.996 0 0 0-.991-.991h-4.019zm4.299 12.712l-4.576 4.577c-.393.392-.261.711.279.711h4.018a.996.996 0 0 0 .991-.99v-4.019c0-.33-.118-.507-.298-.507-.117 0-.26.074-.414.228z"/></g></svg>' }, props));
};
exports.default = MediaServicesActualSizeIcon;