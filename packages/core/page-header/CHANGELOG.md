# @atlaskit/page-header

## 4.0.2
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/page@6.0.3
  - @atlaskit/single-select@4.0.3
  - @atlaskit/inline-edit@5.0.2
  - @atlaskit/input@2.0.2
  - @atlaskit/field-text@5.0.3
  - @atlaskit/button@7.2.5
  - @atlaskit/breadcrumbs@4.1.3
  - @atlaskit/docs@3.0.4

## 4.0.1
- [patch] Fixed alignment of title for page-header [a9f95f6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a9f95f6)

## 4.0.0
- [major] Titles no longer truncate by default. Use the truncateTitle prop instead. [6879ef0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6879ef0)

## 3.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 2.4.2

## 2.4.1
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 2.4.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 2.3.0
- [minor] Add disableTitleStyles prop to enable the composition of components that may be affected by the default heading styles. This is a stop-gap measure until we can make a breaking change to update the API to inherently facilitate this. [0866a89](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0866a89)

## 2.2.2
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 2.2.1
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 2.2.0
- [minor] Update readme and docs [7a53047](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7a53047)

## 2.1.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 2.0.7

- [patch] Update dependencies [623f8ca](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/623f8ca)

## 2.0.6

## 2.0.5

## 2.0.4

## 2.0.3

## 2.0.2

## 2.0.1
- [patch] Migrated page-header to mk2. Fixed breadcrumbs main entry point [51bf0c7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/51bf0c7)
- [patch] Migrated page-header to mk2. Fixed breadcrumbs main entry point [51bf0c7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/51bf0c7)

## 1.0.0