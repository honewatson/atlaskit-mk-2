# @atlaskit/inline-dialog

## 6.0.2
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/icon@11.3.0
  - @atlaskit/single-select@4.0.3
  - @atlaskit/button@7.2.5
  - @atlaskit/theme@3.2.2
  - @atlaskit/docs@3.0.4
  - @atlaskit/layer@3.1.1

## 6.0.1

## 6.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 5.3.2
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 5.3.1
- [patch] Removed focus ring from inline-dialogs focused via the mouse [a17adde](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a17adde)

## 5.3.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 5.2.2
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 5.2.1
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 5.2.0
- [minor] Updated inline-dialog to include boundaries element prop, updated Layer to have dynamic boolean escapeWithReference property, updated modal-dialog Content component with overflow-x:hidden' [cb72752](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/cb72752)

## 5.1.2



- [patch] Revert name of stateless export to InlineEditStateless [fffacd6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/fffacd6)

## 5.1.1
- [patch] Resolved low hanging flow errors in field-base field-text comment icon item and website, $ [007de27](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/007de27)

## 5.1.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 5.0.7
- [patch] moved react-dom to peer dependency [214dd1f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/214dd1f)

## 5.0.6
- [patch] migrated inline dialog from ak to mk2 [9feaa91](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9feaa91)

## 5.0.5 (2017-11-24)




* bug fix; prevent inline-dialog from closing when event is prevented and prevent default for c (issues closed: ak-3870) ([8ae0c3b](https://bitbucket.org/atlassian/atlaskit/commits/8ae0c3b))
## 5.0.4 (2017-10-26)

* bug fix; fix to rebuild stories ([793b2a7](https://bitbucket.org/atlassian/atlaskit/commits/793b2a7))
## 5.0.3 (2017-10-22)

* bug fix; update styled component dependency and react peerDep ([39f3286](https://bitbucket.org/atlassian/atlaskit/commits/39f3286))

## 5.0.2 (2017-10-03)

* bug fix; refactored how inline-dialog handles max-width in order to better support scrollable ([20b62a6](https://bitbucket.org/atlassian/atlaskit/commits/20b62a6))


## 5.0.1 (2017-08-21)

* bug fix; fix PropTypes warning ([040d579](https://bitbucket.org/atlassian/atlaskit/commits/040d579))
## 5.0.0 (2017-08-11)

* bug fix; fix the theme-dependency ([db90333](https://bitbucket.org/atlassian/atlaskit/commits/db90333))

* bug fix; inline-dialog: fix vertical padding ([49d8c5d](https://bitbucket.org/atlassian/atlaskit/commits/49d8c5d))
* bug fix; inline-dialog: updates from design review ([ff38fa2](https://bitbucket.org/atlassian/atlaskit/commits/ff38fa2))
* breaking; affects internal styled-components implementation ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))
* breaking; implement dark mode theme ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))







## 4.0.0 (2017-08-11)

* bug fix; inline-dialog: fix vertical padding ([49d8c5d](https://bitbucket.org/atlassian/atlaskit/commits/49d8c5d))
* bug fix; inline-dialog: updates from design review ([ff38fa2](https://bitbucket.org/atlassian/atlaskit/commits/ff38fa2))
* breaking; affects internal styled-components implementation ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))
* breaking; implement dark mode theme ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))








## 3.6.2 (2017-07-27)


* fix; rename jsnext:main to jsnext:experimental:main temporarily ([c7508e0](https://bitbucket.org/atlassian/atlaskit/commits/c7508e0))

## 3.6.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 3.3.0 (2017-07-17)

## 3.3.0 (2017-07-17)

## 3.3.0 (2017-07-17)


* fix; rerelease, failed prepublish scripts ([5fd82f8](https://bitbucket.org/atlassian/atlaskit/commits/5fd82f8))

## 3.3.0 (2017-07-17)


* feature; added ES module builds to dist and add jsnext:main to most ADG packages ([ea76507](https://bitbucket.org/atlassian/atlaskit/commits/ea76507))

## 3.2.1 (2017-07-13)


* fix; add prop-types as a dependency to avoid React 15.x warnings ([92598eb](https://bitbucket.org/atlassian/atlaskit/commits/92598eb))

## 3.2.0 (2017-05-16)


* feature; bumping util-shared-styles in inline-dialog ([429e23a](https://bitbucket.org/atlassian/atlaskit/commits/429e23a))

## 3.1.2 (2017-04-27)


* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 3.1.1 (2017-04-26)


* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))

## 3.1.0 (2017-04-18)

## 3.0.0 (2017-04-11)


* feature; allow inline dialog to be closed via document click ([bdc7dc5](https://bitbucket.org/atlassian/atlaskit/commits/bdc7dc5))


* breaking; added ReactDOM as a peerDependency

ISSUES CLOSED: AK-2069

## 2.0.0 (2017-03-31)


null refactor the inline-dialog component to use styled-components ([85294ec](https://bitbucket.org/atlassian/atlaskit/commits/85294ec))


* feature; Allow an array of positions to be passed to the shouldFlip property ([1a2a3f6](https://bitbucket.org/atlassian/atlaskit/commits/1a2a3f6))


* breaking; added peerDependency "styled-components”

ISSUES CLOSED: AK-1988, AK-1996

## 1.1.0 (2017-03-28)


* feature; add onContentClick property to inline-dialog ([ff7404e](https://bitbucket.org/atlassian/atlaskit/commits/ff7404e))
* feature; add onContentFocus and onContentBlur properties to inline-dialog ([9cc1663](https://bitbucket.org/atlassian/atlaskit/commits/9cc1663))

## 1.0.7 (2017-03-23)


* fix; Empty commit to release the component ([49c08ee](https://bitbucket.org/atlassian/atlaskit/commits/49c08ee))

## 1.0.5 (2017-03-21)

## 1.0.5 (2017-03-21)


* fix; maintainers for all the packages were added ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))

## 1.0.4 (2017-02-20)


* fix; use correctly scoped package names in npm docs ([91dbd2f](https://bitbucket.org/atlassian/atlaskit/commits/91dbd2f))

## 1.0.3 (2017-02-09)


* fix; avoiding binding render to this ([40c9951](https://bitbucket.org/atlassian/atlaskit/commits/40c9951))

## 1.0.2 (2017-02-07)


* fix; allow inline dialog trigger to take full width ([38325fc](https://bitbucket.org/atlassian/atlaskit/commits/38325fc))

## 1.0.1 (2017-02-06)


* fix; Updates package to use scoped ak packages ([38fca7c](https://bitbucket.org/atlassian/atlaskit/commits/38fca7c))
