# @atlaskit/input

## 2.0.2
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/field-base@9.0.3
  - @atlaskit/theme@3.2.2

## 2.0.1

## 2.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 1.7.1
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 1.7.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 1.6.6
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 1.6.5
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 1.6.4

- [patch] Fixed font-size in Input component [bebf0ab](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bebf0ab)

## 1.6.3


- [patch] Minor documentation fixes [f0e96bd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f0e96bd)

## 1.6.2
- [patch] Move input to new repository [0d7c5fd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0d7c5fd)

## 1.6.1 (2017-11-15)

* bug fix; bumping internal dependencies to latest major version ([d5b6fb9](https://bitbucket.org/atlassian/atlaskit/commits/d5b6fb9))
## 1.6.0 (2017-10-24)


* feature; expose select method on Input class (issues closed: ak-3727) ([82aa750](https://bitbucket.org/atlassian/atlaskit/commits/82aa750))
## 1.5.4 (2017-10-22)

* bug fix; update styled component dependency and react peerDep ([39f3286](https://bitbucket.org/atlassian/atlaskit/commits/39f3286))

## 1.5.3 (2017-09-05)

* bug fix; input component now also inherits the letter-spacing from the CSS reset. ([cf96c4c](https://bitbucket.org/atlassian/atlaskit/commits/cf96c4c))
* bug fix; inputs in IE11 no longer change size when used with inline-edit ([029cca6](https://bitbucket.org/atlassian/atlaskit/commits/029cca6))





## 1.5.2 (2017-07-27)


* fix; rename jsnext:main to jsnext:experimental:main temporarily ([c7508e0](https://bitbucket.org/atlassian/atlaskit/commits/c7508e0))

## 1.5.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 1.2.0 (2017-07-17)

## 1.2.0 (2017-07-17)

## 1.2.0 (2017-07-17)


* fix; rerelease, failed prepublish scripts ([5fd82f8](https://bitbucket.org/atlassian/atlaskit/commits/5fd82f8))

## 1.2.0 (2017-07-17)


* feature; added ES module builds to dist and add jsnext:main to most ADG packages ([ea76507](https://bitbucket.org/atlassian/atlaskit/commits/ea76507))

## 1.1.3 (2017-07-13)


* fix; add prop-types as a dependency to avoid React 15.x warnings ([92598eb](https://bitbucket.org/atlassian/atlaskit/commits/92598eb))

## 1.1.2 (2017-04-27)


* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 1.1.1 (2017-04-26)


* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))

## 1.1.0 (2017-03-29)


* feature; use styled-components to style the input component ([79a9e0b](https://bitbucket.org/atlassian/atlaskit/commits/79a9e0b))

## 1.0.10 (2017-03-21)

## 1.0.10 (2017-03-21)


* fix; maintainers for all the packages were added ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))

## 1.0.9 (2017-02-28)


* fix; dummy commit to release stories ([3df5d9f](https://bitbucket.org/atlassian/atlaskit/commits/3df5d9f))

## 1.0.7 (2017-02-28)


* fix; dummy commit to fix broken stories and missing registry pages ([a31e92a](https://bitbucket.org/atlassian/atlaskit/commits/a31e92a))

## 1.0.7 (2017-02-28)


* fix; dummy commit to release stories for components ([a105c02](https://bitbucket.org/atlassian/atlaskit/commits/a105c02))

## 1.0.6 (2017-02-28)


* fix; removes jsdoc annoations and moves them to usage.md ([0df7be1](https://bitbucket.org/atlassian/atlaskit/commits/0df7be1))

## 1.0.5 (2017-02-27)


* empty commit to make components release themselves ([5511fbe](https://bitbucket.org/atlassian/atlaskit/commits/5511fbe))

## 1.0.4 (2017-02-22)


* fix; Adds description to input package.json ([643ebed](https://bitbucket.org/atlassian/atlaskit/commits/643ebed))
* fix; Adds docs to input component ([46dc9b4](https://bitbucket.org/atlassian/atlaskit/commits/46dc9b4))

## 1.0.3 (2017-02-16)


* fix; fixes a bug when inline-edit switch to read view programatically ([3a93e51](https://bitbucket.org/atlassian/atlaskit/commits/3a93e51))

## 1.0.2 (2017-02-09)


* fix; avoiding binding render to this ([40c9951](https://bitbucket.org/atlassian/atlaskit/commits/40c9951))

## 1.0.1 (2017-02-06)


* fix; Updates package to use scoped ak packages ([107381d](https://bitbucket.org/atlassian/atlaskit/commits/107381d))
