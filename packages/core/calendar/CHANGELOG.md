# @atlaskit/calendar

## 5.0.4
- [patch] Fix issue causing a mouseup to select a date. It is now bound to click. [1cd0b7e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1cd0b7e)

## 5.0.3
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/icon@11.3.0
  - @atlaskit/button@7.2.5
  - @atlaskit/theme@3.2.2
  - @atlaskit/docs@3.0.4

## 5.0.2

## 5.0.1
- [patch] Better styles for disabled dates [866c497](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/866c497)

## 5.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 4.0.0
- [major] QoL and consistency changes to the calendar and datetime-picker APIs. Added the ability to specify a string to the DateTimePicker component. Remove stateless components and make each component stateless or stateful using the controlled / uncontrolled pattern. Misc prop renames for consistency. [ab21d8e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ab21d8e)

## 3.2.1
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 3.2.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 3.1.3
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 3.1.2
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 3.1.1
- [patch] Flatten examples for easier consumer use [145b632](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/145b632)

## 3.1.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 3.0.13
- [patch] Fixed issue where hovering over a disabled date would not show a disabled cursor. [5c21f9b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5c21f9b)

## 3.0.12
- [patch] Fix calendar dates not being selectable in IE11 [a65e3b0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a65e3b0)

## 3.0.11

## 3.0.10
- [patch] stopped disabled dates from triggering onClick prop [3b42698](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3b42698)

## 3.0.9
- [patch] did some clean up with accessibility of calendar [48797f2](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/48797f2)

## 3.0.8

## 3.0.7

## 3.0.6
- [patch] bump icon dependency [da14956](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/da14956)
- [patch] bump icon dependency [da14956](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/da14956)

## 3.0.5

## 3.0.4
- [patch] Use correct dependencies  [7b178b1](7b178b1)
- [patch] Use correct dependencies  [7b178b1](7b178b1)
- [patch] Adding responsive behavior to the editor. [e0d9867](e0d9867)
- [patch] Adding responsive behavior to the editor. [e0d9867](e0d9867)
