# @atlaskit/inline-message

## 4.0.2
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/icon@11.3.0
  - @atlaskit/inline-dialog@6.0.2
  - @atlaskit/button@7.2.5
  - @atlaskit/theme@3.2.2
  - @atlaskit/docs@3.0.4

## 4.0.1

## 4.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 3.2.2
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 3.2.1
- [patch] added truncating logic for text in inline message [0b59d44](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0b59d44)

## 3.2.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 3.1.3
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 3.1.2
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 3.1.1
- [patch] Resolved low hanging flow errors in field-base field-text comment icon item and website, $ [007de27](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/007de27)

## 3.1.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 3.0.6
- [patch] updated icons for error, info and udpated size of icon for confirmation [824c40d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/824c40d)

## 3.0.5
- [patch] migrate inline-message from ak to mk-2 [2323022](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/2323022)

## 3.0.4 (2017-10-26)

* bug fix; fix to rebuild stories ([793b2a7](https://bitbucket.org/atlassian/atlaskit/commits/793b2a7))

## 3.0.3 (2017-10-22)

* bug fix; update styled component dependency and react peerDep ([39f3286](https://bitbucket.org/atlassian/atlaskit/commits/39f3286))

## 3.0.2 (2017-10-15)

* bug fix; update dependencies for react 16 compatibility ([fc47c94](https://bitbucket.org/atlassian/atlaskit/commits/fc47c94))
## 3.0.1 (2017-09-21)

* bug fix; removed unnecessary horizontal spacing on inline-messages (issues closed: ak-2603) ([d608d79](https://bitbucket.org/atlassian/atlaskit/commits/d608d79))

## 3.0.0 (2017-08-24)


* feature; remove util-shared-styles ([d0331cf](https://bitbucket.org/atlassian/atlaskit/commits/d0331cf))

* breaking; Update inline message to include dark mode theming ([dfc37f8](https://bitbucket.org/atlassian/atlaskit/commits/dfc37f8))
* breaking; update inline message to include dark mode theming (issues closed: #ak-3237) ([dfc37f8](https://bitbucket.org/atlassian/atlaskit/commits/dfc37f8))






## 2.5.2 (2017-07-27)


* fix; rename jsnext:main to jsnext:experimental:main temporarily ([c7508e0](https://bitbucket.org/atlassian/atlaskit/commits/c7508e0))

## 2.5.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 2.2.0 (2017-07-17)

## 2.2.0 (2017-07-17)

## 2.2.0 (2017-07-17)


* fix; rerelease, failed prepublish scripts ([5fd82f8](https://bitbucket.org/atlassian/atlaskit/commits/5fd82f8))

## 2.2.0 (2017-07-17)


* feature; added ES module builds to dist and add jsnext:main to most ADG packages ([ea76507](https://bitbucket.org/atlassian/atlaskit/commits/ea76507))

## 2.1.1 (2017-06-20)


* fix; bump inline-message dependencies to latest ([f705804](https://bitbucket.org/atlassian/atlaskit/commits/f705804))

## 2.1.0 (2017-06-08)


* fix; add prop-types as a dependency to avoid React 15.x warnings ([92598eb](https://bitbucket.org/atlassian/atlaskit/commits/92598eb))


* feature; refactor inline-message to styled-components ([5144fc5](https://bitbucket.org/atlassian/atlaskit/commits/5144fc5))

## 2.0.0 (2017-05-11)


* fix; update dependencies ([6b6f84e](https://bitbucket.org/atlassian/atlaskit/commits/6b6f84e))


null bumps util-shared-styles and inline-dialog dependencies to latest versions ([b02a77d](https://bitbucket.org/atlassian/atlaskit/commits/b02a77d))


* breaking; Introduces react-dom as a peer dependency (to satisfy inline-dialogs peer dep)

ISSUES CLOSED: AK-2361

## 1.2.2 (2017-04-27)


* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 1.2.1 (2017-04-26)


* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))

## 1.2.0 (2017-04-20)


* feature; removed explicit style! imports, set style-loader in webpack config ([891fc3c](https://bitbucket.org/atlassian/atlaskit/commits/891fc3c))

## 1.1.3 (2017-03-23)


* fix; Empty commit to release the component ([49c08ee](https://bitbucket.org/atlassian/atlaskit/commits/49c08ee))

## 1.1.1 (2017-03-21)

## 1.1.1 (2017-03-21)


* fix; maintainers for all the packages were added ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))

## 1.1.0 (2017-03-03)


* feature; allow positioning of dialog for inline messages ([bdaa4d6](https://bitbucket.org/atlassian/atlaskit/commits/bdaa4d6))

## 1.0.2 (2017-02-09)


* fix; avoiding binding render to this ([40c9951](https://bitbucket.org/atlassian/atlaskit/commits/40c9951))

## 1.0.1 (2017-02-06)


* fix; Updates packages to use scoped ak packages ([f16660f](https://bitbucket.org/atlassian/atlaskit/commits/f16660f))
