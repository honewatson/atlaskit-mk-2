# @atlaskit/progress-tracker

## 2.0.2
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/page@6.0.3
  - @atlaskit/theme@3.2.2
  - @atlaskit/docs@3.0.4

## 2.0.1

## 2.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 1.3.1
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 1.3.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 1.2.2
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 1.2.1
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 1.2.0
- [minor] Updated examples to use named import for clarity [541f2fb](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/541f2fb)

## 1.1.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 1.0.2
- [patch] Added animations to progress bar [369af3a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/369af3a)

## 0.2.0
- [minor] Initial Release of Atlaskit Progress Tracker [3b3c9df](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3b3c9df)
