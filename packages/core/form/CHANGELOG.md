# @atlaskit/form

## 1.0.4
- [patch] Updated dependencies [6859cf6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6859cf6)
  - @atlaskit/field-text@5.1.0
  - @atlaskit/field-text-area@2.1.0

## 1.0.3
- [patch] Fix pinned field-text dep [050ad7b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/050ad7b)

## 1.0.2
- [patch] Updated dependencies [d05b9e5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d05b9e5)
  - @atlaskit/select@3.0.0
  - @atlaskit/datetime-picker@4.0.0

## 1.0.1

## 1.0.0
- [patch] Form developer preview [d8b2b03](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d8b2b03)
- [major] Form package developer preview release [9b28847](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9b28847)
