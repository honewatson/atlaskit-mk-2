# @atlaskit/blanket

## 5.0.2
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/button@7.2.5
  - @atlaskit/theme@3.2.2
  - @atlaskit/docs@3.0.4

## 5.0.1

## 5.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 4.1.1
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 4.1.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 4.0.9
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 4.0.8
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 4.0.7


- [patch] Minor documentation fixes [f0e96bd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f0e96bd)

## 4.0.6

- [patch] Migrate Navigation from Ak repo to ak mk 2 repo, Fixed flow typing inconsistencies in ak mk 2 [bdeef5b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bdeef5b)

## 4.0.5
- [patch] Resolved low hanging flow errors in field-base field-text comment icon item and website, $ [007de27](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/007de27)

## 4.0.4
- [patch] Migration of Blanket to mk2 repo [1c55d97](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1c55d97)

## 4.0.3 (2017-12-21)

* bug fix; Minor change to storybook to test new s3 bucket ([97cbb7d](https://bitbucket.org/atlassian/atlaskit/commits/97cbb7d))
## 4.0.2 (2017-11-27)

* bug fix; bump theme dependency to 2.2.0 (issues closed: nav-27) ([2b71345](https://bitbucket.org/atlassian/atlaskit/commits/2b71345))

## 4.0.1 (2017-11-15)

* bug fix; bumping internal dependencies to latest major versions ([288935a](https://bitbucket.org/atlassian/atlaskit/commits/288935a))
## 4.0.0 (2017-11-14)

* bug fix; implemented code review comments: using theme package to get layer value and removed ([b1a84f5](https://bitbucket.org/atlassian/atlaskit/commits/b1a84f5))
* breaking; added z-index to blanket ([4665973](https://bitbucket.org/atlassian/atlaskit/commits/4665973))
* breaking; aK-3851-fix added z-index to blanket, reverts b344810 (issues closed: ak-3851) ([4665973](https://bitbucket.org/atlassian/atlaskit/commits/4665973))
## 3.0.3 (2017-10-26)

* bug fix; fix to rebuild stories ([793b2a7](https://bitbucket.org/atlassian/atlaskit/commits/793b2a7))
## 3.0.2 (2017-10-22)

* bug fix; update dependencies for react-16 ([077d1ad](https://bitbucket.org/atlassian/atlaskit/commits/077d1ad))
## 3.0.1 (2017-10-13)



* bug fix; fix Blanket stories ([b9ac4ab](https://bitbucket.org/atlassian/atlaskit/commits/b9ac4ab))
* bug fix; other fix for other issue ([014f79c](https://bitbucket.org/atlassian/atlaskit/commits/014f79c))
* bug fix; fix render issue for Blanket storybook ([b277ff3](https://bitbucket.org/atlassian/atlaskit/commits/b277ff3))
## 3.0.0 (2017-08-31)

* breaking; removed z-index ([b344810](https://bitbucket.org/atlassian/atlaskit/commits/b344810))
* breaking; removed z-index for integration with @atlaskit/layer-manager ([b344810](https://bitbucket.org/atlassian/atlaskit/commits/b344810))
* feature; update to use @atlaskit/theme - now supports dark mode ([422c74e](https://bitbucket.org/atlassian/atlaskit/commits/422c74e))


## 2.4.3 (2017-08-09)

* bug fix; bump util-shared-styles dependency to latest to reduce app bundle sizes (issues closed: ak-3252) ([dbc406c](https://bitbucket.org/atlassian/atlaskit/commits/dbc406c))





## 2.4.2 (2017-07-27)


* fix; rename jsnext:main to jsnext:experimental:main temporarily ([c7508e0](https://bitbucket.org/atlassian/atlaskit/commits/c7508e0))

## 2.4.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 2.1.0 (2017-07-17)

## 2.1.0 (2017-07-17)

## 2.1.0 (2017-07-17)


* fix; rerelease, failed prepublish scripts ([5fd82f8](https://bitbucket.org/atlassian/atlaskit/commits/5fd82f8))

## 2.1.0 (2017-07-17)


* feature; added ES module builds to dist and add jsnext:main to most ADG packages ([ea76507](https://bitbucket.org/atlassian/atlaskit/commits/ea76507))

## 2.0.5 (2017-07-13)


* fix; testing releasing more than 5 packages at a time ([e69b832](https://bitbucket.org/atlassian/atlaskit/commits/e69b832))
* fix; add prop-types as a dependency to avoid React 15.x warnings ([92598eb](https://bitbucket.org/atlassian/atlaskit/commits/92598eb))

## 2.0.4 (2017-04-27)


* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 2.0.3 (2017-04-26)


* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))

## 2.0.2 (2017-03-29)


* fix; repush stories for broken releases ([cde4000](https://bitbucket.org/atlassian/atlaskit/commits/cde4000))

## 2.0.1 (2017-03-28)


* fix; update blanket with new structure, use new readme story component ([2ecfd11](https://bitbucket.org/atlassian/atlaskit/commits/2ecfd11))

## 2.0.0 (2017-03-27)


null refactor the blanket component to use styled-components ([62dc8f2](https://bitbucket.org/atlassian/atlaskit/commits/62dc8f2))


* breaking; removed dependency "classnames", added peerDependency "styled-components"

## 1.2.4 (2017-03-23)


* fix; Empty commit to release the component ([49c08ee](https://bitbucket.org/atlassian/atlaskit/commits/49c08ee))

## 1.2.2 (2017-03-21)

## 1.2.2 (2017-03-21)


* fix; maintainers for all the packages were added ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))

## 1.2.1 (2017-03-14)

## 1.2.0 (2017-02-27)


* feature; added `canClickThroughBlanket` prop to Blanket to make it possible to animate ([cacb5cb](https://bitbucket.org/atlassian/atlaskit/commits/cacb5cb))

## 1.1.0 (2017-02-22)


* feature; add canClickThrough to blanket ([c2e31aa](https://bitbucket.org/atlassian/atlaskit/commits/c2e31aa))

## 1.0.2 (2017-02-10)


* fix; Dummy commit to release components to registry ([5bac43b](https://bitbucket.org/atlassian/atlaskit/commits/5bac43b))

## 1.0.1 (2017-02-06)


* fix; bumps deps to use scoped packages ([9384221](https://bitbucket.org/atlassian/atlaskit/commits/9384221))
