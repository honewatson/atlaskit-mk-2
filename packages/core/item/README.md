# Item

This is a generic Item component, designed to be composed declaratively into other components.

Item is generally a layout component, concerned with visual presentation of the content provided via props.

## Extending Item behaviour

If you need the Item component to have additional behaviour, it is recommended that you wrap Item in a [higher-order component](https://facebook.github.io/react/docs/higher-order-components.html).

## Item theming

The default Item component has a default theme, which you can override using the styled-components ThemeProvider component.

For details on how to do this, take a look at the ItemThemeDemo [here](https://atlaskit.atlassian.com) for this component.

## Try it out

Interact with a [live demo of the @atlaskit/item component](https://atlaskit.atlassian.com).

## Installation

```sh
yarn add @atlaskit/item
```
