# @atlaskit/analytics

## 3.0.1
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/docs@3.0.4

## 3.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 2.4.5

- [patch] Add "sideEffects: false" to AKM2 packages to allow consumer's to tree-shake [c3b018a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c3b018a)

## 2.4.4
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 2.4.3
- [patch] updated repository url to atlassian/atlaskit [9fb2698](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9fb2698)

## 2.4.2
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 2.4.1
- [patch] Fix clean props type [b0a2cf0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b0a2cf0)
- [patch] Add a typescript definition for clean props [88cc4be](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/88cc4be)

## 2.4.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 2.3.5
- [patch] Added type fields to analytics package.json [286cfc4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/286cfc4)

## 2.3.4
- [patch] Restored type exports in analytics [7a4a9d2](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7a4a9d2)

## 2.3.3
- [patch] Migrated to the mk2 repo [eba7e6a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/eba7e6a)

## 2.3.2 (2017-10-24)

* bug fix; update the cleanprops function ([e27ace3](https://bitbucket.org/atlassian/atlaskit/commits/e27ace3))
## 2.3.1 (2017-10-22)

* bug fix; update dependencies for react-16 ([077d1ad](https://bitbucket.org/atlassian/atlaskit/commits/077d1ad))
## 2.3.0 (2017-10-20)

* bug fix; lint fix. Remove bad comment. ([311ca12](https://bitbucket.org/atlassian/atlaskit/commits/311ca12))
* feature; support bubble events between react trees via the AnalyticsDelegate (issues closed: fs-1424) ([f6af591](https://bitbucket.org/atlassian/atlaskit/commits/f6af591))
## 2.2.2 (2017-10-17)

* bug fix; fix console errors ([aa2f97f](https://bitbucket.org/atlassian/atlaskit/commits/aa2f97f))
## 2.2.1 (2017-10-09)

* bug fix; added missing type definition file in package analytics build. ([24df127](https://bitbucket.org/atlassian/atlaskit/commits/24df127))
## 2.2.0 (2017-10-06)

* feature; analytics library support for passing decorated data to stores (issues closed: ak-3614) ([43e3314](https://bitbucket.org/atlassian/atlaskit/commits/43e3314))
## 2.1.0 (2017-10-05)

* feature; action/decision related analytics (issues closed: fs-1290) ([38ade4e](https://bitbucket.org/atlassian/atlaskit/commits/38ade4e))


## 2.0.0 (2017-09-13)

* breaking; AnalyticsListener and AnalyticsDecorator now only accepts one child and will throw an error if ([92f1b3f](https://bitbucket.org/atlassian/atlaskit/commits/92f1b3f))
* breaking; analyticsListener and AnalyticsDecorator now only accepts one child (issues closed: ak-2048) ([92f1b3f](https://bitbucket.org/atlassian/atlaskit/commits/92f1b3f))
## 1.1.2 (2017-09-05)


* bug fix; expose innerRef on WithAnalytics ([3f8a210](https://bitbucket.org/atlassian/atlaskit/commits/3f8a210))
## 1.1.1 (2017-08-11)

* bug fix; fix the theme-dependency ([db90333](https://bitbucket.org/atlassian/atlaskit/commits/db90333))



## 1.1.0 (2017-08-03)



* feature; allow analytics components to set a default analyticsId and analyticsData (issues closed: ak-3162) ([6c5ce68](https://bitbucket.org/atlassian/atlaskit/commits/6c5ce68))



## 1.0.3 (2017-07-31)

* bug fix; fixed analytics partial string match (issues closed: ak-3072) ([328a204](https://bitbucket.org/atlassian/atlaskit/commits/328a204))



## 1.0.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 1.0.0 (2017-07-18)


* fix; correct entrypoint for ak:webpack:raw ([f76254f](https://bitbucket.org/atlassian/atlaskit/commits/f76254f))
* fix; remove anayticsDelay feature from initial scope ([dcd471c](https://bitbucket.org/atlassian/atlaskit/commits/dcd471c))


* feature; add analytics package ([19fda60](https://bitbucket.org/atlassian/atlaskit/commits/19fda60))
