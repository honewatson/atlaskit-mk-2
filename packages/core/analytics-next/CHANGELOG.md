# @atlaskit/analytics-next

## 2.1.3
- [patch] Removed ambient typescript type declaration file from analytics-next - this may be a breaking change for typescript consumers [290d804](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/290d804)
- [none] Updated dependencies [290d804](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/290d804)

## 2.1.2
- [patch] Fix prop callbacks specified in the create event map to not change reference values each render and instead only update when the original prop callback changes [586a80c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/586a80c)
- [none] Updated dependencies [586a80c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/586a80c)

## 2.1.1
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/field-text@5.0.3
  - @atlaskit/button@7.2.5
  - @atlaskit/docs@3.0.4

## 2.1.0

- [minor] Export cleanProps function that can be used to strip analytics props provided by our HOCs, useful when spreading props to a child element [973d6ea](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/973d6ea)

## 2.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 1.1.10
- [patch] Adjusted exports to prevent attempted exporting of flow types in built code. [183ee96](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/183ee96)

## 1.1.9
- [patch] Updates flow types of withAnalyticsEvents and withAnalyticsContext HOCs [26778bc](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/26778bc)
- [patch] Uses element config flow type with button deprecation warnings hoc [a9aa90a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a9aa90a)

## 1.1.8

- [patch] Add "sideEffects: false" to AKM2 packages to allow consumer's to tree-shake [c3b018a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c3b018a)

## 1.1.7
- [patch] Fix/revert TS TDs in analytics-next [1284d32](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1284d32)

## 1.1.6
- [patch] Fix analytics-next TS type definition [9faaa5f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9faaa5f)
- [patch] Fix analytics-next TS type definition [7e26229](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7e26229)

## 1.1.5
- [patch] Add analytics events for click and show actions of media-card [031d5da](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/031d5da)
- [patch] Add analytics events for click and show actions of media-card [b361185](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b361185)

## 1.1.4
- [patch] fixes problem with withAnalyticsEvents HOC passing old function props to wrapped component [c88b030](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c88b030)

## 1.1.3
- [patch] adds displayName to analytics HOCs [f69ccad](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f69ccad)

## 1.1.2
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 1.1.1
- [patch] Remove min requirement of node 8 for analytics-next [c864671](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c864671)

## 1.1.0
- [minor] adds createAndFireEvent utility method and updates docs [24a93fc](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/24a93fc)

## 1.0.3
- [patch] fixes flow type problem with wrapping stateless functional components in withAnalyticsEvents [8344ffb](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8344ffb)

## 1.0.2
- [patch] Adds action key to analytics payload type [7deeaef](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7deeaef)

## 1.0.1
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 1.0.0
- [major] release @atlaskit/analytics-next package [80695ea](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/80695ea)
