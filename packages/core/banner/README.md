# Banner

This is a React component that displays a prominent banner message. It is
intended to be used at the top of the page.

![Example Banner](https://i.imgur.com/5N9j2tp.png)

## Try it out

Detailed docs and example usage can be found
[here](https://atlaskit.atlassian.com/packages/core/banner).

## Installation

```sh
npm install @atlaskit/banner
```
