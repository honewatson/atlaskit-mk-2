# @atlaskit/banner

## 5.0.2
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/icon@11.3.0
  - @atlaskit/button@7.2.5
  - @atlaskit/theme@3.2.2
  - @atlaskit/docs@3.0.4

## 5.0.1

## 5.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 4.2.2
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 4.2.1
- [patch] Update links in documentation [c4f7497](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c4f7497)

## 4.2.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 4.1.4
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 4.1.3
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 4.1.2
- [patch] Flatten examples for easier consumer use [145b632](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/145b632)

## 4.1.1
- [patch] Resolved low hanging flow errors in field-base field-text comment icon item and website, $ [007de27](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/007de27)

## 4.1.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 4.0.7

## 4.0.6

## 4.0.5
- [patch] Migrate banner to new atlaskit repo [0d035b2](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0d035b2)

## 4.0.4 (2017-11-21)

* bug fix; bumping internal dependencies to latest major version ([3d49e8e](https://bitbucket.org/atlassian/atlaskit/commits/3d49e8e))
## 4.0.3 (2017-10-26)

* bug fix; fix to rebuild stories ([793b2a7](https://bitbucket.org/atlassian/atlaskit/commits/793b2a7))
## 4.0.2 (2017-10-22)

* bug fix; update dependencies for react-16 ([077d1ad](https://bitbucket.org/atlassian/atlaskit/commits/077d1ad))
## 4.0.1 (2017-09-18)

* bug fix; update darkmode colors ([7ae860b](https://bitbucket.org/atlassian/atlaskit/commits/7ae860b))
## 4.0.0 (2017-08-11)

* bug fix; fix the theme-dependency ([db90333](https://bitbucket.org/atlassian/atlaskit/commits/db90333))

* breaking; affects internal styled-components implementation ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))
* breaking; implement dark mode theme ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))
* feature; dark mode for Banner, plus loads of cleanup in docs/stories ([3f03b9a](https://bitbucket.org/atlassian/atlaskit/commits/3f03b9a))
* bug fix; remove ak-icon and update @atlaskit/icon to latest in package.json ([8018cf0](https://bitbucket.org/atlassian/atlaskit/commits/8018cf0))





## 3.0.0 (2017-08-11)

* breaking; affects internal styled-components implementation ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))
* breaking; implement dark mode theme ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))
* feature; dark mode for Banner, plus loads of cleanup in docs/stories ([3f03b9a](https://bitbucket.org/atlassian/atlaskit/commits/3f03b9a))
* bug fix; remove ak-icon and update @atlaskit/icon to latest in package.json ([8018cf0](https://bitbucket.org/atlassian/atlaskit/commits/8018cf0))






## 2.4.2 (2017-07-27)


* fix; rename jsnext:main to jsnext:experimental:main temporarily ([c7508e0](https://bitbucket.org/atlassian/atlaskit/commits/c7508e0))

## 2.4.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 2.1.0 (2017-07-17)

## 2.1.0 (2017-07-17)

## 2.1.0 (2017-07-17)


* fix; rerelease, failed prepublish scripts ([5fd82f8](https://bitbucket.org/atlassian/atlaskit/commits/5fd82f8))

## 2.1.0 (2017-07-17)


* feature; added ES module builds to dist and add jsnext:main to most ADG packages ([ea76507](https://bitbucket.org/atlassian/atlaskit/commits/ea76507))

## 2.0.5 (2017-07-13)


* fix; testing releasing more than 5 packages at a time ([e69b832](https://bitbucket.org/atlassian/atlaskit/commits/e69b832))
* fix; add prop-types as a dependency to avoid React 15.x warnings ([92598eb](https://bitbucket.org/atlassian/atlaskit/commits/92598eb))

## 2.0.4 (2017-05-09)


* fix; update dependencies ([3295727](https://bitbucket.org/atlassian/atlaskit/commits/3295727))

## 2.0.3 (2017-04-28)


* fix; banner max-height now a pixel value that matches the line-height and padding correct ([85bac54](https://bitbucket.org/atlassian/atlaskit/commits/85bac54))

## 2.0.2 (2017-04-27)


* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 2.0.1 (2017-04-26)


* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))

## 2.0.0 (2017-03-31)

## 1.0.8 (2017-03-29)


* fix; repush stories for broken releases ([cde4000](https://bitbucket.org/atlassian/atlaskit/commits/cde4000))


null refactor the banner component to use styled-components ([a72af4c](https://bitbucket.org/atlassian/atlaskit/commits/a72af4c))


* breaking; removed dependency "classnames", added peerDependency "styled-components"

ISSUES CLOSED: AK-1980

## 1.0.7 (2017-03-28)


* fix; update banner readme story to use new readme component ([fa7b692](https://bitbucket.org/atlassian/atlaskit/commits/fa7b692))

## 1.0.6 (2017-03-23)


* fix; Empty commit to release the component ([49c08ee](https://bitbucket.org/atlassian/atlaskit/commits/49c08ee))

## 1.0.4 (2017-03-21)

## 1.0.4 (2017-03-21)


* fix; maintainers for all the packages were added ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))

## 1.0.3 (2017-02-20)


* fix; use correctly scoped package names in npm docs ([91dbd2f](https://bitbucket.org/atlassian/atlaskit/commits/91dbd2f))

## 1.0.2 (2017-02-10)


* fix; Dummy commit to release components to registry ([5bac43b](https://bitbucket.org/atlassian/atlaskit/commits/5bac43b))

## 1.0.1 (2017-02-06)


* fix; Updates deps to use scoped packages ([f773486](https://bitbucket.org/atlassian/atlaskit/commits/f773486))
