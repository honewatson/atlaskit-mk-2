# @atlaskit/global-navigation

## 0.0.5
- [patch] Updated dependencies [3882051](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3882051)
  - @atlaskit/navigation-next@0.1.1

## 0.0.4
- [patch] Bumping dep on navigation-next [548787e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/548787e)

## 0.0.3
- [patch] Rename props to be in sync with navigation-next package [1fde1da](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1fde1da)

## 0.0.2

- [patch] Add global-navigation package [41a4d1c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/41a4d1c)
- [patch] Updated dependencies [7c99742](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7c99742)
  - @atlaskit/navigation-next@0.0.7
