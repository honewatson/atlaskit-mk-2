# @atlaskit/tooltip

## 9.2.1
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/icon@11.3.0
  - @atlaskit/layer-manager@3.0.4
  - @atlaskit/button@7.2.5
  - @atlaskit/theme@3.2.2
  - @atlaskit/docs@3.0.4

## 9.2.0
- [minor] add delay prop to tooltip. still defaults to 300ms. [66c6264](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/66c6264)
- [none] Updated dependencies [66c6264](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/66c6264)

## 9.1.6

## 9.1.5
- [patch] Fix long words in tooltip content overflowing the tooltip, they will now wrap. [b2967ef](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b2967ef)

## 9.1.4
- [patch] Fix tooltips sometimes not hiding when rapidly switching between them [760f6a0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/760f6a0)

## 9.1.3
- [patch] Fix react warnings caused when unmounting a tooltip when it is queued for show/hide [6d9cc52](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6d9cc52)

## 9.1.2
- [patch] Fix tooltip scroll listeners not being removed properly and an edgecase viewport autoflip issue [0a3ccc9](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0a3ccc9)

## 9.1.1
- [patch] Fix viewport edge collision detection for non-mouse positions in some cases and improve detection to include scrollbars [e66bce5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e66bce5)

## 9.1.0
- [patch] Improve viewport edge collision detection. Tooltips will now shift along the secondary position axis (e.g. left/right when position is top/bottom) to show within viewport. Fix auto flip occurring incorrectly in these situations as well. [ebf331a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ebf331a)
- [minor] Add new 'mouse' value for position prop and mousePosition prop to allow the tooltip to display relative to the mouse. [1d5577d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1d5577d)

## 9.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 8.4.2
- [patch] Makes packages Flow types compatible with version 0.67 [25daac0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/25daac0)

## 8.4.1
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 8.4.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 8.3.2
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 8.3.1
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 8.3.0
- [minor] update atlaskit/theme to 2.3.2 [3795197](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3795197)

## 8.2.1
- [patch] Flatten examples for easier consumer use [145b632](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/145b632)

## 8.2.0
- [minor] new prop component to open custom tooltip [3f892d5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3f892d5)

## 8.1.1
- [patch] Resolved low hanging flow errors in field-base field-text comment icon item and website, $ [007de27](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/007de27)

## 8.1.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 8.0.13

## 8.0.12
- [patch] replace internal deprecation warning hoc with package [c399777](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c399777)

## 8.0.11

## 8.0.10
- [patch] AK-4064 ensure unmountComponentAtNode is called for components rendered via ReactDOM.render [e3153c3](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e3153c3)
- [patch] AK-4064 ensure unmountComponentAtNode is called for components rendered via ReactDOM.render [e3153c3](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e3153c3)

## 8.0.9



- [patch] remove unused button dependency and corrected themes type [3475dd6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3475dd6)

## 8.0.8

## 8.0.7

## 8.0.6
- [patch] bump icon dependency [da14956](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/da14956)
- [patch] bump icon dependency [da14956](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/da14956)

## 8.0.5

## 8.0.4

## 8.0.3
- [patch] Use correct dependencies  [7b178b1](7b178b1)
- [patch] Use correct dependencies  [7b178b1](7b178b1)
- [patch] Adding responsive behavior to the editor. [e0d9867](e0d9867)
- [patch] Adding responsive behavior to the editor. [e0d9867](e0d9867)

## 8.0.0 (2017-11-10)

* This was an accidental release - do no use, go directly to 8.0.1

## 7.0.0 (2017-11-10)

* added flow types
* rewritten the logic for positioning tooltips, removed Popper.js
* uses @atlaskit/layer-manager to render outside app context/stack
* removed stateless component
* \`description\` has been renamed to \`content\`

## 6.2.2 (2017-10-26)

* bug fix; fix to rebuild stories ([793b2a7](https://bitbucket.org/atlassian/atlaskit/commits/793b2a7))
## 6.2.1 (2017-10-22)

* bug fix; update styled components dep and react peerDep ([5539ada](https://bitbucket.org/atlassian/atlaskit/commits/5539ada))
## 6.2.0 (2017-10-18)




* feature; add trigger prop to tooltip ([7721243](https://bitbucket.org/atlassian/atlaskit/commits/7721243))
* feature; use mouseEnter and mouseLeave instead of mouseOver and mouseOut ([55cf15e](https://bitbucket.org/atlassian/atlaskit/commits/55cf15e))
## 6.1.0 (2017-10-18)



* feature; add trigger prop to tooltip ([7721243](https://bitbucket.org/atlassian/atlaskit/commits/7721243))
* feature; use mouseEnter and mouseLeave instead of mouseOver and mouseOut ([55cf15e](https://bitbucket.org/atlassian/atlaskit/commits/55cf15e))
## 6.0.0 (2017-08-30)

* breaking; The tooltip trigger is now wrapped in a div with 'display: inline-block' applied. Previously it was ([de263e5](https://bitbucket.org/atlassian/atlaskit/commits/de263e5))
* breaking; tooltip now disappears as soon as the mouse leaves the trigger (issues closed: ak-1834) ([de263e5](https://bitbucket.org/atlassian/atlaskit/commits/de263e5))

## 5.0.1 (2017-08-21)

* bug fix; fix PropTypes warning ([040d579](https://bitbucket.org/atlassian/atlaskit/commits/040d579))
## 5.0.0 (2017-08-11)

* bug fix; fix the theme-dependency ([db90333](https://bitbucket.org/atlassian/atlaskit/commits/db90333))

* breaking; affects internal styled-components implementation ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))
* breaking; implement dark mode theme ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))
* feature; updated dark colors for Tooltip ([8fbbb8c](https://bitbucket.org/atlassian/atlaskit/commits/8fbbb8c))






* feature; new theme methods ([3656ee3](https://bitbucket.org/atlassian/atlaskit/commits/3656ee3))


* feature; add dark mode support to tooltip ([aa87b89](https://bitbucket.org/atlassian/atlaskit/commits/aa87b89))
## 4.0.0 (2017-08-11)

* breaking; affects internal styled-components implementation ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))
* breaking; implement dark mode theme ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))
* feature; updated dark colors for Tooltip ([8fbbb8c](https://bitbucket.org/atlassian/atlaskit/commits/8fbbb8c))






* feature; new theme methods ([3656ee3](https://bitbucket.org/atlassian/atlaskit/commits/3656ee3))


* feature; add dark mode support to tooltip ([aa87b89](https://bitbucket.org/atlassian/atlaskit/commits/aa87b89))

## 3.4.2 (2017-07-27)


* fix; rename jsnext:main to jsnext:experimental:main temporarily ([c7508e0](https://bitbucket.org/atlassian/atlaskit/commits/c7508e0))

## 3.4.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 3.1.0 (2017-07-17)

## 3.1.0 (2017-07-17)

## 3.1.0 (2017-07-17)


* fix; rerelease, failed prepublish scripts ([5fd82f8](https://bitbucket.org/atlassian/atlaskit/commits/5fd82f8))

## 3.1.0 (2017-07-17)


* feature; added ES module builds to dist and add jsnext:main to most ADG packages ([ea76507](https://bitbucket.org/atlassian/atlaskit/commits/ea76507))

## 2.0.0 (2017-07-17)


null replace LESS with SC ([d1b5911](https://bitbucket.org/atlassian/atlaskit/commits/d1b5911))


* breaking; named export "Tooltip" is now "TooltipStateless". prop "visible" is now "isVisible"

ISSUES CLOSED: AK-2059

## 1.2.1 (2017-07-13)


* fix; add prop-types as a dependency to avoid React 15.x warnings ([92598eb](https://bitbucket.org/atlassian/atlaskit/commits/92598eb))

## 1.2.0 (2017-05-10)


* feature; bump layer version in [@atlaskit](https://github.com/atlaskit)/tooltip ([cfa9903](https://bitbucket.org/atlassian/atlaskit/commits/cfa9903))

## 1.1.2 (2017-04-27)


* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 1.1.1 (2017-04-26)


* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))

## 1.1.0 (2017-04-20)


* feature; removed explicit style! imports, set style-loader in webpack config ([891fc3c](https://bitbucket.org/atlassian/atlaskit/commits/891fc3c))

## 1.0.8 (2017-04-04)


* fix; adds defensive code to allow testing in mocha/jsdom ([2eaab5b](https://bitbucket.org/atlassian/atlaskit/commits/2eaab5b))

## 1.0.6 (2017-03-21)

## 1.0.6 (2017-03-21)


* fix; maintainers for all the packages were added ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))

## 1.0.5 (2017-03-06)

## 1.0.4 (2017-02-28)


* fix; prevent word wrapping of tooltip for TextAdvancdFormatting elements ([31b51a4](https://bitbucket.org/atlassian/atlaskit/commits/31b51a4))
* fix; removes jsdoc annotations and moves content to usage.md ([2d794cd](https://bitbucket.org/atlassian/atlaskit/commits/2d794cd))
* fix; dummy commit to release stories ([3df5d9f](https://bitbucket.org/atlassian/atlaskit/commits/3df5d9f))

## 1.0.3 (2017-02-20)


* Add missing TS definition for tooltip ([aae714d](https://bitbucket.org/atlassian/atlaskit/commits/aae714d))
* Add TS definition for tooltip ([5c023e9](https://bitbucket.org/atlassian/atlaskit/commits/5c023e9))
* Use atlaskit tooltips instead of browser native tooltips ([d0018eb](https://bitbucket.org/atlassian/atlaskit/commits/d0018eb))

## 1.0.2 (2017-02-07)


* fix; Updates package to use scoped ak packages ([73d1427](https://bitbucket.org/atlassian/atlaskit/commits/73d1427))