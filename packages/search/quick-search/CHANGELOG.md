# @atlaskit/quick-search

## 1.7.1
- [patch] Fix searchbox height. [586f868](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/586f868)
- [none] Updated dependencies [586f868](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/586f868)

## 1.7.0
- [minor] Change subText rendering of ObjectResult [dc6bfd7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/dc6bfd7)
- [none] Updated dependencies [dc6bfd7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/dc6bfd7)

## 1.6.0
- [minor] UI Polish [65392e5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/65392e5)
- [none] Updated dependencies [65392e5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/65392e5)

## 1.5.0
- [minor] Add Avatar prop to quick search result types [aefb12d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/aefb12d)

## 1.4.2
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/icon@11.3.0
  - @atlaskit/navigation@31.0.4
  - @atlaskit/item@6.0.3
  - @atlaskit/field-base@9.0.3
  - @atlaskit/theme@3.2.2
  - @atlaskit/avatar@10.0.6
  - @atlaskit/docs@3.0.4
  - @atlaskit/analytics@3.0.1

## 1.4.1

## 1.4.0
- [minor] Add support for linkComponent prop [4c9e683](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4c9e683)

## 1.3.2
- [patch] Remove dependency on navigation [756d26b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/756d26b)

## 1.3.1

## 1.3.0
- [minor] Remove unecessary dependencies [3bd4dd8](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3bd4dd8)

## 1.2.0
- [minor] Add missing AkSearch legacy export [1b40786](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1b40786)

## 1.1.0
- [minor] Fix wrongly named legacy exports [e7baf6b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e7baf6b)

## 1.0.0
- [major] Extract quick-search from @atlaskit/navigation [dda7e32](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/dda7e32)
