# @atlaskit/global-search

## 3.3.3
- [patch] Update people results link path in global search [be04163](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/be04163)

## 3.3.2
- [patch] Remove SVG react warnings. [89ce7d7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/89ce7d7)

## 3.3.1
- [patch] Updated dependencies [586f868](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/586f868)
  - @atlaskit/quick-search@1.7.1

## 3.3.0
- [minor] Consistent avatar sizes [0f80c65](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0f80c65)

## 3.2.1
- [patch]  [f87724e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f87724e)

## 3.2.0
- [minor] Updated dependencies [65392e5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/65392e5)
  - @atlaskit/quick-search@1.6.0

## 3.1.2
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/icon@11.3.0
  - @atlaskit/quick-search@1.4.2
  - @atlaskit/page@6.0.3
  - @atlaskit/navigation@31.0.4
  - @atlaskit/button@7.2.5
  - @atlaskit/theme@3.2.2
  - @atlaskit/avatar@10.0.6
  - @atlaskit/docs@3.0.4
  - @atlaskit/util-service-support@2.0.7
  - @atlaskit/analytics@3.0.1

## 3.1.1

## 3.1.0

## 3.0.5

## 3.0.4
- [patch] Link to people search includes query [6d2e946](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6d2e946)

## 3.0.3

## 3.0.2
- [patch] Rename EmptyState component to NoResults [cb73105](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/cb73105)

## 3.0.1
- [patch] Simplify tests [d2437e9](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d2437e9)

## 3.0.0
- [major] Building blocks to support Confluence mode. "context" is a required prop now. [a5f1cef](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a5f1cef)

## 2.1.1
- [patch] Bumping dependency on avatar [ca55e9c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ca55e9c)

## 2.1.0
- [minor] Remove dependency on navigation. [0ae3355](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0ae3355)

## 2.0.1
- [patch] Added missing dependencies and added lint rule to catch them all [0672503](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0672503)

## 2.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 1.7.0
- [minor] Show empty state when no results were found at all [398901a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/398901a)

## 1.6.0

## 1.5.0
- [minor] Show error state when searches fail [4fbbb29](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4fbbb29)

## 1.4.0
- [minor] Improve rendering of Jira issues [7f28452](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7f28452)

## 1.3.1
- [patch] Clean up CSS for examples [1b7ffce](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1b7ffce)

## 1.3.0
- [minor] Adds search attribution analytics for confluence search results. [2d73f50](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/2d73f50)

## 1.2.0
- [minor] Remove environment prop and replace with explicit service url overrides. [b5bd020](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b5bd020)

## 1.1.1
- [patch] Support rendering of Jira results [0381f03](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0381f03)

## 1.1.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 1.0.1
- [patch] Update atlaskit/theme version [679e68a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/679e68a)

## 1.0.0
- [major] First release of global-search [7fcd54e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7fcd54e)
