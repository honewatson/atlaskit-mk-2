# @atlaskit/editor-json-transformer

## 3.0.7




- [none] Updated dependencies [febc44d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/febc44d)
  - @atlaskit/editor-core@72.0.0
  - @atlaskit/editor-test-helpers@4.0.0
  - @atlaskit/util-data-test@9.1.4
  - @atlaskit/editor-common@10.0.0
- [none] Updated dependencies [714ab32](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/714ab32)
  - @atlaskit/util-data-test@9.1.4
  - @atlaskit/editor-common@10.0.0
  - @atlaskit/editor-test-helpers@4.0.0
  - @atlaskit/editor-core@72.0.0
- [patch] Updated dependencies [84f6f91](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/84f6f91)
  - @atlaskit/util-data-test@9.1.4
  - @atlaskit/editor-common@10.0.0
  - @atlaskit/editor-test-helpers@4.0.0
  - @atlaskit/editor-core@72.0.0
- [patch] Updated dependencies [9041d71](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9041d71)
  - @atlaskit/util-data-test@9.1.4
  - @atlaskit/editor-common@10.0.0
  - @atlaskit/editor-test-helpers@4.0.0
  - @atlaskit/editor-core@72.0.0

## 3.0.6






- [none] Updated dependencies [8fd4dd1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8fd4dd1)
  - @atlaskit/editor-test-helpers@3.1.8
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/editor-core@71.4.0
  - @atlaskit/editor-common@9.3.9
- [none] Updated dependencies [74f84c6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/74f84c6)
  - @atlaskit/editor-common@9.3.9
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/editor-test-helpers@3.1.8
  - @atlaskit/editor-core@71.4.0
- [none] Updated dependencies [92cdf83](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/92cdf83)
  - @atlaskit/editor-core@71.4.0
  - @atlaskit/editor-test-helpers@3.1.8
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/editor-common@9.3.9
- [none] Updated dependencies [4151cc5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4151cc5)
  - @atlaskit/editor-core@71.4.0
  - @atlaskit/editor-test-helpers@3.1.8
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/editor-common@9.3.9
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/editor-core@71.4.0
  - @atlaskit/editor-test-helpers@3.1.8
  - @atlaskit/editor-common@9.3.9
  - @atlaskit/docs@3.0.4
- [patch] Updated dependencies [89146bf](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/89146bf)
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/editor-core@71.4.0
  - @atlaskit/editor-test-helpers@3.1.8
  - @atlaskit/editor-common@9.3.9

## 3.0.5
- [patch] ED-4336 support loading dynamic/"auto" tables from confluence to fixed-width tables [0c2f72a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0c2f72a)

## 3.0.4

## 3.0.3

## 3.0.2

## 3.0.1
- [patch] Added missing dependencies and added lint rule to catch them all [0672503](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0672503)

## 3.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 2.5.21
- [patch] change table node builder constructor for tests, remove tableWithAttrs [cf43535](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/cf43535)

## 2.5.20

## 2.5.19

## 2.5.18
- [patch] Upgrading ProseMirror Libs [35d14d5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/35d14d5)

## 2.5.17

- [patch] Add "sideEffects: false" to AKM2 packages to allow consumer's to tree-shake [c3b018a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c3b018a)

## 2.5.16

## 2.5.15

## 2.5.14

## 2.5.13

## 2.5.12

## 2.5.11

## 2.5.10

## 2.5.9

## 2.5.8

## 2.5.7

## 2.5.6
- [patch] JSON encoding results in invalid ADF for table nodes [8a8d663](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8a8d663)

## 2.5.5
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 2.5.4

## 2.5.3
- [patch] bump editor-common to 6.1.2 [bb7802e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bb7802e)

## 2.5.2

## 2.5.1

## 2.5.0
- [minor] Implement JSONTransformer::parse to enable ADF -> PM conversion [c8c3c9e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c8c3c9e)

## 2.4.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 2.3.1

## 2.3.0

## 2.2.0

## 2.1.11

## 2.1.10

- [patch] Change to use editor-core instead of editor-bitbucket for examples [aa0c0ac](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/aa0c0ac)

## 2.1.9

## 2.1.8




- [patch] Move prosemirror-model to be a dev-dependency [206ce2f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/206ce2f)
- [patch] Move prosemirror-model to be a dev-dependency [206ce2f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/206ce2f)

## 2.1.7

## 2.1.6

## 2.1.5

## 2.1.4

## 2.1.3

## 2.1.2

## 2.1.1

## 2.1.0

## 2.0.5

## 2.0.4

## 2.0.3
- [patch] Fix dependencies [9f9de42](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9f9de42)
- [patch] Fix dependencies [9f9de42](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9f9de42)

## 2.0.2
- [patch] Fix of the build scripts for editor-*-transformer packages [59b4ea5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/59b4ea5)
- [patch] Fix of the build scripts for editor-*-transformer packages [59b4ea5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/59b4ea5)

## 2.0.1
- [patch] Fixed linting [5ebf75d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5ebf75d)
- [patch] Fixed linting [5ebf75d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5ebf75d)

## 2.0.0
- [major] Adding separate transformer packages. [f734c01](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f734c01)
- [major] Adding separate transformer packages. [f734c01](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f734c01)
