# @atlaskit/editor-bitbucket-transformer

## 3.1.2
- [patch] Fix image parsing [5fc825b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5fc825b)
- [none] Updated dependencies [5fc825b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5fc825b)

## 3.1.1




- [none] Updated dependencies [febc44d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/febc44d)
  - @atlaskit/editor-core@72.0.0
  - @atlaskit/editor-test-helpers@4.0.0
  - @atlaskit/util-data-test@9.1.4
  - @atlaskit/editor-common@10.0.0
- [none] Updated dependencies [714ab32](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/714ab32)
  - @atlaskit/util-data-test@9.1.4
  - @atlaskit/editor-common@10.0.0
  - @atlaskit/editor-test-helpers@4.0.0
  - @atlaskit/editor-core@72.0.0
- [patch] Updated dependencies [84f6f91](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/84f6f91)
  - @atlaskit/util-data-test@9.1.4
  - @atlaskit/editor-common@10.0.0
  - @atlaskit/editor-test-helpers@4.0.0
  - @atlaskit/editor-core@72.0.0
- [patch] Updated dependencies [9041d71](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9041d71)
  - @atlaskit/util-data-test@9.1.4
  - @atlaskit/editor-common@10.0.0
  - @atlaskit/editor-test-helpers@4.0.0
  - @atlaskit/editor-core@72.0.0

## 3.1.0
- [minor] Support external media in bitbucket transformer and image uploader [8fd4dd1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8fd4dd1)





- [none] Updated dependencies [8fd4dd1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8fd4dd1)
  - @atlaskit/editor-test-helpers@3.1.8
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/editor-core@71.4.0
  - @atlaskit/editor-common@9.3.9
- [none] Updated dependencies [74f84c6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/74f84c6)
  - @atlaskit/editor-common@9.3.9
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/editor-test-helpers@3.1.8
  - @atlaskit/editor-core@71.4.0
- [none] Updated dependencies [92cdf83](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/92cdf83)
  - @atlaskit/editor-core@71.4.0
  - @atlaskit/editor-test-helpers@3.1.8
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/editor-common@9.3.9
- [none] Updated dependencies [4151cc5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4151cc5)
  - @atlaskit/editor-core@71.4.0
  - @atlaskit/editor-test-helpers@3.1.8
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/editor-common@9.3.9
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/editor-core@71.4.0
  - @atlaskit/editor-test-helpers@3.1.8
  - @atlaskit/editor-common@9.3.9
  - @atlaskit/docs@3.0.4
- [patch] Updated dependencies [89146bf](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/89146bf)
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/editor-core@71.4.0
  - @atlaskit/editor-test-helpers@3.1.8
  - @atlaskit/editor-common@9.3.9

## 3.0.4

## 3.0.3

## 3.0.2

## 3.0.1
- [patch] Added missing dependencies and added lint rule to catch them all [0672503](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0672503)

## 3.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 2.7.12
- [patch] change table node builder constructor for tests, remove tableWithAttrs [cf43535](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/cf43535)

## 2.7.11

## 2.7.10

## 2.7.9

## 2.7.8
- [patch] Upgrading ProseMirror Libs [35d14d5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/35d14d5)

## 2.7.7

## 2.7.6

- [patch] Add "sideEffects: false" to AKM2 packages to allow consumer's to tree-shake [c3b018a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c3b018a)

## 2.7.5

## 2.7.4

## 2.7.3

## 2.7.2

## 2.7.1

## 2.7.0
- [minor] Use `data-` attribute to recognise auto-linked Bitbucket URLs instead of rel="nofollow" [d9df1c4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d9df1c4)

## 2.6.8

## 2.6.7

## 2.6.6

## 2.6.5

## 2.6.4

## 2.6.3

## 2.6.2

## 2.6.1

## 2.6.0

## 2.5.6
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 2.5.5

## 2.5.4

## 2.5.3
- [patch] bump editor-common to 6.1.2 [bb7802e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bb7802e)

## 2.5.2
- [patch] bump mention to 9.1.1 to fix mention autocomplete bug [c7708c6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c7708c6)

## 2.5.1

## 2.5.0
- [minor] Fixed issue causing leading ' characters to be escaped [1b2140c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1b2140c)
- [patch] Fix images with underscore in URL [42ed524](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/42ed524)

## 2.4.2

## 2.4.1

## 2.4.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 2.3.1

## 2.3.0

## 2.2.0

## 2.1.12
- [patch] Fix dependencies in CI [670e930](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/670e930)

## 2.1.11

## 2.1.10

- [patch] Change to use editor-core instead of editor-bitbucket for examples [aa0c0ac](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/aa0c0ac)

## 2.1.9

## 2.1.8

## 2.1.7

## 2.1.6

## 2.1.5

## 2.1.4

## 2.1.3

## 2.1.2

## 2.1.1


- [patch] Change @atlaskit/editor-core to be only a dev dependency [765b8ff](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/765b8ff)
- [patch] Change @atlaskit/editor-core to be only a dev dependency [765b8ff](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/765b8ff)

## 2.1.0

## 2.0.5

## 2.0.4

## 2.0.3

## 2.0.2
- [patch] Fix of the build scripts for editor-*-transformer packages [59b4ea5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/59b4ea5)
- [patch] Fix of the build scripts for editor-*-transformer packages [59b4ea5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/59b4ea5)

## 2.0.1
- [patch] Fixed linting [5ebf75d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5ebf75d)
- [patch] Fixed linting [5ebf75d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5ebf75d)

## 2.0.0
- [major] Adding separate transformer packages. [f734c01](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f734c01)
- [major] Adding separate transformer packages. [f734c01](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f734c01)
