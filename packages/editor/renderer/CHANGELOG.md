# @atlaskit/renderer

## 17.0.8
- [patch] Updated dependencies [35d547f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/35d547f)
  - @atlaskit/media-card@28.0.5
  - @atlaskit/editor-common@10.1.4

## 17.0.7
- [patch] Fix mediaSingle [179332e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/179332e)
- [none] Updated dependencies [179332e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/179332e)

## 17.0.6
- [patch] Updated dependencies [41eb1c1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/41eb1c1)
  - @atlaskit/editor-common@10.1.3

## 17.0.5
- [patch] ED-4447 Fix image breakout rendering [b73e05d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b73e05d)
- [none] Updated dependencies [b73e05d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b73e05d)
  - @atlaskit/editor-common@10.1.2

## 17.0.4
- [patch] Updated dependencies [639ae5e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/639ae5e)
  - @atlaskit/mention@12.0.2
  - @atlaskit/util-data-test@9.1.7
  - @atlaskit/editor-common@10.1.1

## 17.0.3
- [patch] Updated dependencies [0edc6c8](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0edc6c8)

## 17.0.2
- [patch] Updated dependencies [758b342](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/758b342)
  - @atlaskit/task-decision@6.0.7

## 17.0.1



- [none] Updated dependencies [ba702bc](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ba702bc)
  - @atlaskit/mention@12.0.0
  - @atlaskit/util-data-test@9.1.6
  - @atlaskit/editor-common@10.0.3
- [patch] Updated dependencies [db1bafa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/db1bafa)
  - @atlaskit/mention@12.0.0
  - @atlaskit/util-data-test@9.1.6
  - @atlaskit/editor-common@10.0.3
- [none] Updated dependencies [f150242](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f150242)
  - @atlaskit/mention@12.0.0
  - @atlaskit/util-data-test@9.1.6
  - @atlaskit/editor-common@10.0.3

## 17.0.0

- [patch] ED-4570, application card without icon should render properly. [714ab32](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/714ab32)


- [none] Updated dependencies [febc44d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/febc44d)
  - @atlaskit/editor-test-helpers@4.0.0
  - @atlaskit/task-decision@6.0.6
  - @atlaskit/util-data-test@9.1.4
  - @atlaskit/editor-common@10.0.0
  - @atlaskit/editor-json-transformer@3.0.7
- [none] Updated dependencies [714ab32](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/714ab32)
  - @atlaskit/task-decision@6.0.6
  - @atlaskit/util-data-test@9.1.4
  - @atlaskit/editor-common@10.0.0
  - @atlaskit/editor-test-helpers@4.0.0
  - @atlaskit/editor-json-transformer@3.0.7
- [patch] Updated dependencies [84f6f91](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/84f6f91)
  - @atlaskit/media-test-helpers@13.0.0
  - @atlaskit/media-core@18.1.0
  - @atlaskit/util-data-test@9.1.4
  - @atlaskit/task-decision@6.0.6
  - @atlaskit/editor-json-transformer@3.0.7
  - @atlaskit/editor-common@10.0.0
  - @atlaskit/media-filmstrip@8.0.7
  - @atlaskit/media-card@28.0.0
  - @atlaskit/editor-test-helpers@4.0.0
- [major] Updated dependencies [9041d71](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9041d71)
  - @atlaskit/media-test-helpers@13.0.0
  - @atlaskit/media-core@18.1.0
  - @atlaskit/util-data-test@9.1.4
  - @atlaskit/task-decision@6.0.6
  - @atlaskit/editor-json-transformer@3.0.7
  - @atlaskit/editor-common@10.0.0
  - @atlaskit/media-filmstrip@8.0.7
  - @atlaskit/media-card@28.0.0
  - @atlaskit/editor-test-helpers@4.0.0

## 16.3.0
- [minor] Adds support for adfStage [4b303ce](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4b303ce)
- [none] Updated dependencies [4b303ce](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4b303ce)

## 16.2.6






- [none] Updated dependencies [8fd4dd1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8fd4dd1)
  - @atlaskit/editor-test-helpers@3.1.8
  - @atlaskit/task-decision@6.0.5
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/mention@11.1.4
  - @atlaskit/editor-json-transformer@3.0.6
  - @atlaskit/editor-common@9.3.9
- [none] Updated dependencies [74f84c6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/74f84c6)
  - @atlaskit/editor-common@9.3.9
  - @atlaskit/task-decision@6.0.5
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/mention@11.1.4
  - @atlaskit/editor-test-helpers@3.1.8
  - @atlaskit/editor-json-transformer@3.0.6
- [none] Updated dependencies [92cdf83](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/92cdf83)
  - @atlaskit/editor-test-helpers@3.1.8
  - @atlaskit/task-decision@6.0.5
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/mention@11.1.4
  - @atlaskit/editor-common@9.3.9
  - @atlaskit/editor-json-transformer@3.0.6
- [none] Updated dependencies [4151cc5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4151cc5)
  - @atlaskit/editor-test-helpers@3.1.8
  - @atlaskit/task-decision@6.0.5
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/mention@11.1.4
  - @atlaskit/editor-common@9.3.9
  - @atlaskit/editor-json-transformer@3.0.6
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/task-decision@6.0.5
  - @atlaskit/mention@11.1.4
  - @atlaskit/editor-json-transformer@3.0.6
  - @atlaskit/editor-test-helpers@3.1.8
  - @atlaskit/editor-common@9.3.9
  - @atlaskit/icon@11.3.0
  - @atlaskit/media-filmstrip@8.0.6
  - @atlaskit/media-card@27.1.4
  - @atlaskit/media-test-helpers@12.0.4
  - @atlaskit/media-core@18.0.3
  - @atlaskit/theme@3.2.2
  - @atlaskit/code@4.0.4
  - @atlaskit/docs@3.0.4
- [patch] Updated dependencies [89146bf](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/89146bf)
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/task-decision@6.0.5
  - @atlaskit/mention@11.1.4
  - @atlaskit/editor-json-transformer@3.0.6
  - @atlaskit/editor-test-helpers@3.1.8
  - @atlaskit/editor-common@9.3.9

## 16.2.5
- [patch] Renamed smart card components and exposed inline smart card views [1094bb6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1094bb6)
- [patch] Updated dependencies [1094bb6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1094bb6)
  - @atlaskit/media-card@27.1.3

## 16.2.4
- [patch] Adding nested ul support [ce87690](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ce87690)
- [none] Updated dependencies [ce87690](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ce87690)

## 16.2.3
- [patch] Disable overlay for mediaSingle [147bc84](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/147bc84)
- [none] Updated dependencies [147bc84](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/147bc84)
  - @atlaskit/editor-common@9.3.6

## 16.2.2
- [patch] ED-4120 support placeholder text in renderer [616a6a5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/616a6a5)
- [patch] Updated dependencies [616a6a5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/616a6a5)
  - @atlaskit/editor-common@9.3.5

## 16.2.1


- [patch] Updated dependencies [3ef21cd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3ef21cd)
  - @atlaskit/editor-common@9.3.4
- [none] Updated dependencies [9591127](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9591127)
  - @atlaskit/editor-common@9.3.4

## 16.2.0
- [minor] Set line-height based on appearance [b21cd55](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b21cd55)
- [none] Updated dependencies [b21cd55](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b21cd55)

## 16.1.3
- [patch] Add a blank space between mention and text in text renderer [940ecc7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/940ecc7)

## 16.1.2

## 16.1.1

## 16.1.0
- [minor] Adding support for external images [9935105](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9935105)

## 16.0.9

## 16.0.8
- [patch] ED-4568, adding support for panel types success and error in renderer. [1aef8d2](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1aef8d2)

## 16.0.7

## 16.0.6

## 16.0.5
- [patch] Fix rendering of multiple text nodes in inline code [9ee5612](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ee5612)

## 16.0.4
- [patch] CFE-1078: Add the type of extension to the call to extension handler [4db252c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4db252c)

## 16.0.3

## 16.0.2
- [patch] Use node type as fallback behavior for unsupported node [090d962](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/090d962)

## 16.0.1
- [patch] Always wrap text [bcd3361](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bcd3361)

## 16.0.0

## 15.0.2

## 15.0.1
- [patch] Added missing dependencies and added lint rule to catch them all [0672503](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0672503)

## 15.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 14.0.0
- [major] Generic Text Serializer [1549347](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1549347)

## 13.3.4

## 13.3.3
- [patch] support table colwidth in renderer, fix other table properties in email renderer [f78bef4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f78bef4)

## 13.3.2

## 13.3.1

## 13.3.0

## 13.2.0
- [minor] stop creating mediaContext in MediaCard component [ad8c3c0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ad8c3c0)

## 13.1.0

## 13.0.17

## 13.0.16
- [patch] Move types/interfaces for ExtensionHandlers to editor-common [3d26cab](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3d26cab)

## 13.0.15

## 13.0.14
- [patch] Prevent CodeBlocks from overflowing their container [50cc975](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/50cc975)

## 13.0.13
- [patch] Upgrading ProseMirror Libs [35d14d5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/35d14d5)

## 13.0.12

## 13.0.11
- [patch] Adds styling for unknown blocks [5cdc63c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5cdc63c)

## 13.0.10

- [patch] Add "sideEffects: false" to AKM2 packages to allow consumer's to tree-shake [c3b018a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c3b018a)

## 13.0.9
- [patch] Change Media Group resize mode to full-fit from crop [05575db](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/05575db)

## 13.0.8

## 13.0.7

## 13.0.6
- [patch] Add analytics events for click and show actions of media-card [031d5da](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/031d5da)

## 13.0.5

## 13.0.4

## 13.0.3

## 13.0.2
- [patch] Adds margin-top to ApplicationCard, MediaGroup and CodeBlock in renderer content [f2ae5ca](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f2ae5ca)

## 13.0.1

## 13.0.0

## 12.2.1

## 12.2.0
- [minor] E-mail renderer [722657b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/722657b)

## 12.1.0
- [minor] Fixes Media Cards in renderer [064bfb5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/064bfb5)

## 12.0.1

## 12.0.0


- [major] Use media-core as peerDependency [c644812](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c644812)

## 11.5.12

## 11.5.11
- [patch] Add key as an optional parameter to applicationCard actions [28be081](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/28be081)

## 11.5.10

## 11.5.9

## 11.5.8

## 11.5.7

## 11.5.6
- [patch] Fix missing styled-components dependency [64d89c8](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/64d89c8)

## 11.5.5
- [patch] Remove margin from first headings [c8c342d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c8c342d)

## 11.5.4
- [patch] add span and background attribs for table nodes in renderer [8af61df](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8af61df)

## 11.5.3

## 11.5.2

## 11.5.1
- [patch] FS-1461 fixed rendererContext handling in TaskItem [6023540](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6023540)

## 11.5.0
- [minor] FS-1461 objectAri and containerAri are optional in RendererContext [1b20296](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1b20296)

## 11.4.5
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 11.4.4

## 11.4.3



- [patch] added a prop to enable the new applicationCard designs [3057eb2](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3057eb2)

## 11.4.2

## 11.4.1
- [patch] bump editor-common to 6.1.2 [bb7802e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bb7802e)

## 11.4.0
- [minor] Support mediaSingle [400ff24](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/400ff24)

## 11.3.11

## 11.3.10
- [patch] bump mention to 9.1.1 to fix mention autocomplete bug [c7708c6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c7708c6)

## 11.3.9

## 11.3.8

## 11.3.7

- [patch] move MediaItem to renderer, bump icons [5e71725](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5e71725)

## 11.3.6
- [patch] Bump editor versions [afa6885](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/afa6885)

## 11.3.5

## 11.3.4

## 11.3.3

## 11.3.2

## 11.3.1

## 11.3.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 11.2.1

## 11.2.0

## 11.1.1

## 11.1.0

## 11.0.0
- [major] We now use ProseMirror Schema to validate document [d059d6a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d059d6a)

## 10.1.5
- [patch] FS-1581 decreased big emoji size [fe39b29](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/fe39b29)

## 10.1.4

## 10.1.3



- [patch] Fixed stand alone file and link card rendering [9b467a6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9b467a6)

## 10.1.2

## 10.1.1

## 10.1.0



- [minor] Add ADF-Encoder utility to simplify using a transformer with the renderer [5b1ea37](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5b1ea37)
- [minor] Add ADF-Encoder utility to simplify using a transformer with the renderer [5b1ea37](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5b1ea37)

## 10.0.8

## 10.0.7

## 10.0.6

## 10.0.5
- [patch] Only bodiedExtension has content [6d4caae](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6d4caae)
- [patch] Only bodiedExtension has content [6d4caae](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6d4caae)

## 10.0.4

## 10.0.3
- [patch] Bumped task decision version [1180bbe](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1180bbe)
- [patch] Bumped task decision version [1180bbe](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1180bbe)

## 10.0.2

## 10.0.1

## 10.0.0
- [major] Addes in extension node and modify ReactSerializer class construtor to accept an object. [e408698](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e408698)
- [major] Addes in extension node and modify ReactSerializer class construtor to accept an object. [e408698](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e408698)
- [major] Addes in extension node [e52d336](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e52d336)
- [major] Addes in extension node [e52d336](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e52d336)

## 9.1.5

## 9.1.4

## 9.1.3

## 9.1.2

## 9.1.1

## 9.1.0

## 9.0.2

## 9.0.1

## 9.0.0
- [major] Update signature onClick event on filmstrip (renderer) [30bdfcc](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/30bdfcc)
- [major] Update signature onClick event on filmstrip (renderer) [30bdfcc](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/30bdfcc)
- [major] Update signature onClick event on filmstrip (renderer) [dbced25](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/dbced25)
- [major] Update signature onClick event on filmstrip (renderer) [dbced25](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/dbced25)
- [major] Update signature onClick event on filmstrip (renderer) [7ee4743](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7ee4743)
- [major] Update signature onClick event on filmstrip (renderer) [7ee4743](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7ee4743)

## 8.12.0
- [patch] Fix dependencies [9f9de42](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9f9de42)
- [patch] Fix dependencies [9f9de42](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9f9de42)

## 8.11.3

## 8.11.2

## 8.11.1

## 8.11.0
- [minor] Move validators from renderer to editor-common [3e2fd00](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3e2fd00)
- [minor] Move validators from renderer to editor-common [3e2fd00](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3e2fd00)

## 8.10.16

## 8.10.15

## 8.10.14

## 8.10.13
- [patch] Use styled-component for link mark to avoid cascading styles to affect media. [0c9475b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0c9475b)
- [patch] Use styled-component for link mark to avoid cascading styles to affect media. [0c9475b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0c9475b)

## 8.10.12

## 8.10.11

## 8.10.10


- [patch] bump icon dependency [da14956](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/da14956)
- [patch] bump icon dependency [da14956](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/da14956)

## 8.10.9

## 8.10.8

## 8.10.7

## 8.10.6

## 8.10.5

## 8.10.4
- [patch] Fixed stand alone file and link card rendering [9b467a6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9b467a6)
- [patch] Fixed stand alone file and link card rendering [9b467a6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9b467a6)

## 8.10.2
- [patch] Upgrade mention to ^8.1.0 in editor and renderer [48b5af4](48b5af4)
- [patch] Upgrade mention to ^8.1.0 in editor and renderer [48b5af4](48b5af4)

## 8.10.1

## 8.10.0
- [minor] Adding 'image' node for bitbucket consumption; this is unstable and should not be used [590ce41](590ce41)
- [minor] Adding 'image' node for bitbucket consumption; this is unstable and should not be used [590ce41](590ce41)

## 8.9.0

## 8.8.1
- [patch] Use correct dependencies  [7b178b1](7b178b1)
- [patch] Use correct dependencies  [7b178b1](7b178b1)
- [patch] Adding responsive behavior to the editor. [e0d9867](e0d9867)
- [patch] Adding responsive behavior to the editor. [e0d9867](e0d9867)

## 8.8.0
- [minor] Added big emoji rendering logic to renderer [f85c47a](f85c47a)
- [minor] Added big emoji rendering logic to renderer [f85c47a](f85c47a)

## 8.7.1
- [patch] Removing the attrs.language !== undefined validation check for codeBlock nodes [1c20b73](1c20b73)
- [patch] Removing the attrs.language !== undefined validation check for codeBlock nodes [1c20b73](1c20b73)

## 8.7.0
- [minor] Upgrade Media Editor packages [193c8a0](193c8a0)
- [minor] Upgrade Media Editor packages [193c8a0](193c8a0)
