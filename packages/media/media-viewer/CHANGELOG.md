# @atlaskit/media-viewer

## 14.5.1
- [patch] Add issue collector to MVNG [15e0ced](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/15e0ced)

## 14.5.0
- [minor] add custom video player under feature flag [9041109](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9041109)
- [none] Updated dependencies [9041109](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9041109)
  - @atlaskit/media-test-helpers@13.2.0

## 14.4.1
- [patch] Centering navigation arrows vertically in MVNG [d506235](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d506235)

## 14.4.0
- [patch] Fix pageSize in MediaViewer NG [4eac436](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4eac436)
- [minor] show controls when navigation happen in MediaViewer [3917aa6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3917aa6)

## 14.3.1
- [patch] MSW-720 : pass collectionName to all the places for correct auth [f7fa512](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f7fa512)

## 14.3.0
- [minor] Add zoom level for image and document viewer [856dfae](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/856dfae)

## 14.2.0
- [minor] Add keyboard shortcuts to MediaViewer NG [52c56c1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/52c56c1)

## 14.1.0
- [minor] dont hide controls if user is hovering them [f9c7a29](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f9c7a29)

## 14.0.2
- [patch] remove TS casting from MediaViewer [df4da61](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/df4da61)

## 14.0.1
- [patch] Updated dependencies [bd26d3c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bd26d3c)
  - @atlaskit/media-core@18.1.1
  - @atlaskit/media-test-helpers@13.0.1

## 14.0.0


- [major] Updated dependencies [84f6f91](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/84f6f91)
  - @atlaskit/media-test-helpers@13.0.0
  - @atlaskit/media-core@18.1.0
- [patch] Updated dependencies [9041d71](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9041d71)
  - @atlaskit/media-test-helpers@13.0.0
  - @atlaskit/media-core@18.1.0

## 13.8.4
- [patch] Adjust default audio cover [2f37539](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/2f37539)

## 13.8.3
- [patch] Add zooming to document viewer [f76e5d3](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f76e5d3)

## 13.8.2
- [patch] Fix new case of SC component interpolation [accec74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/accec74)

## 13.8.1
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/icon@11.3.0
  - @atlaskit/media-test-helpers@12.0.4
  - @atlaskit/media-core@18.0.3
  - @atlaskit/button@7.2.5
  - @atlaskit/theme@3.2.2
  - @atlaskit/spinner@5.0.2
  - @atlaskit/docs@3.0.4

## 13.8.0
- [minor] show cover for audio files [f830d51](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f830d51)

## 13.7.1
- [patch] Remove component interpolation to be able to integrate with an older version of SC [401db67](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/401db67)

## 13.7.0
- [minor] Add basic zooming to MV [6bd0af4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6bd0af4)

## 13.6.1
- [patch] Use SC style() instead of extend [cc35663](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/cc35663)

## 13.6.0
- [minor] add download button to MediaViewer [ed4ad47](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ed4ad47)

## 13.5.2
- [patch] Fix media-ui dependency version [60f61c5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/60f61c5)

## 13.5.1
- [patch] Add media type metadata to audio in MVNG [8dec6fb](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8dec6fb)

## 13.5.0
- [minor] Add header metadata to MVNG [8aa7812](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8aa7812)

## 13.4.0
- [minor] MediaViewer: toggle UI controls on mouse move [36ec198](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/36ec198)

## 13.3.2
- [patch] Always show MediaViewer close button [9ddeec0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ddeec0)

## 13.3.1
- [patch] use proper collectionName property in MVNG [7815256](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7815256)

## 13.3.0
- [minor] Add collection support to Media Viewer NGwq [6baa5d0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6baa5d0)

## 13.2.3

## 13.2.2
- [patch] Bump z-index of MVNG [7d1f8fb](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7d1f8fb)

## 13.2.1
- [patch] Fix issues with "selectedItem" not being part of the list. [f542262](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f542262)

## 13.2.0
- [minor] General fixes and improvements on MVNG (internal) [117cfc6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/117cfc6)

## 13.1.3
- [patch] update Media Viewer UI to reflect latest designs [fd284c9](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/fd284c9)

## 13.1.2
- [patch] Fix dynamic import in PDF viewer (next gen) [2e37250](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/2e37250)

## 13.1.1

## 13.1.0
- [minor] Add PDF viewer to MVNG [f4dbaa0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f4dbaa0)

## 13.0.0

## 12.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 11.0.7
- [patch] Show upload button during recents load in media picker. + Inprove caching for auth provider used in examples [929731a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/929731a)

## 11.0.6
- [patch] Release first version of image viewer for Media Viewer Next Generation [dd1893a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/dd1893a)

## 11.0.5

- [patch] Add "sideEffects: false" to AKM2 packages to allow consumer's to tree-shake [c3b018a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c3b018a)

## 11.0.4

## 11.0.3

## 11.0.2

## 11.0.1
- [patch] Wire up MVNG with Media Providers [d80c743](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d80c743)

## 11.0.0

## 10.1.0
- [minor] Add Media Viewer Next Gen Feature Flag [5ecb889](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5ecb889)

## 10.0.5

## 10.0.4
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 10.0.3

## 10.0.2
- [patch] Expose analyticsBackend in Media Viewer configuration [3984d91](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3984d91)

## 10.0.1

- [patch] Make MediaListViewer resilient to errors and provide proper view for processing items MSW-25 MSW-348 [1d102d1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1d102d1)

## 10.0.0

## 9.1.1
- [patch] Fixes being unable to close Media Viewer when we open a file that is processing [5f63f6c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5f63f6c)

## 9.1.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 9.0.6
- [patch] Use media-test-helpers instead of hardcoded values [f2b92f8](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f2b92f8)

## 9.0.5
- [patch] Add max-age parameter to media queries in Media Viewer - MSW-328 [4ad5d09](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4ad5d09)

## 9.0.4

## 9.0.3
- [patch]  pass pageSize property down from MediaViewer to MediaCollectionViewer [6fd7dae](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6fd7dae)

## 9.0.2
- [patch] media-core version has changed [9865149](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9865149)

## 9.0.1
- [patch] Migrate MediaViewer to new AK repo [a0bc467](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a0bc467)

## 9.0.0 (2017-11-23)


* breaking; New component introduced: MediaViewer. ([f080bf1](https://bitbucket.org/atlassian/atlaskit/commits/f080bf1))
* breaking; MSW-289 - unify API and exposing a single MediaViewer component (issues closed: msw-289) ([f080bf1](https://bitbucket.org/atlassian/atlaskit/commits/f080bf1))
## 8.1.0 (2017-11-17)

* feature; expand media-viewer peer dependencies range on media-core ([075b97f](https://bitbucket.org/atlassian/atlaskit/commits/075b97f))
* feature; upgrade version of mediapicker to 11.1.6 and media-core to 11.0.0 across packages ([aaa7aa0](https://bitbucket.org/atlassian/atlaskit/commits/aaa7aa0))
## 8.0.0 (2017-09-18)


* breaking; media-core peer dependency has changed to strictly v 10 ([ba73022](https://bitbucket.org/atlassian/atlaskit/commits/ba73022))
* breaking; update media-core and media-test-helpers version ([ba73022](https://bitbucket.org/atlassian/atlaskit/commits/ba73022))
## 7.0.0 (2017-09-18)

* breaking; media-core peer dependency has changed to strictly v 10 ([ba73022](https://bitbucket.org/atlassian/atlaskit/commits/ba73022))
* breaking; update media-core and media-test-helpers version ([ba73022](https://bitbucket.org/atlassian/atlaskit/commits/ba73022))
## 6.1.1 (2017-09-05)

* bug fix; correctly publish type declaration files ([85a5ad2](https://bitbucket.org/atlassian/atlaskit/commits/85a5ad2))


## 6.1.0 (2017-08-11)

* feature; bump :allthethings: ([f4b1375](https://bitbucket.org/atlassian/atlaskit/commits/f4b1375))




## 6.0.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 6.0.0 (2017-05-26)


* fix; fix typo in API: 'occurrenceKey' ([bf68d9a](https://bitbucket.org/atlassian/atlaskit/commits/bf68d9a))


* breaking; 'occurenceKey' renamed to 'occurrenceKey'

## 5.1.0 (2017-05-25)


* feature; add custom configuration to media-viewer ([4a1ad37](https://bitbucket.org/atlassian/atlaskit/commits/4a1ad37))

## 5.0.0 (2017-05-22)


* fix; fix tests ([9d80311](https://bitbucket.org/atlassian/atlaskit/commits/9d80311))


* feature; use media-core 8.0.0 ([0387a76](https://bitbucket.org/atlassian/atlaskit/commits/0387a76))


* breaking; Bump media-core to 8.0.0

## 4.3.1 (2017-04-27)


* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 4.3.0 (2017-04-26)


* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))
* fix; updated media packages key words and maintainers ([01bcbc5](https://bitbucket.org/atlassian/atlaskit/commits/01bcbc5))


* feature; use /image endpoint for images ([c4fdea5](https://bitbucket.org/atlassian/atlaskit/commits/c4fdea5))

## 4.2.0 (2017-04-13)


* feature; add media file list viewer ([c6185c8](https://bitbucket.org/atlassian/atlaskit/commits/c6185c8))

## 4.1.0 (2017-04-12)


* feature; add lazy loading to media collection viewer ([9394310](https://bitbucket.org/atlassian/atlaskit/commits/9394310))

## 4.0.0 (2017-04-11)


* feature; move media-core to peerDependency ([00de0dc](https://bitbucket.org/atlassian/atlaskit/commits/00de0dc))


* breaking; Move media-core to peerDependency in media-viewer

## 3.0.0 (2017-04-10)


* fix; refreshing token in query string no longer  modifies other params ([a2d5030](https://bitbucket.org/atlassian/atlaskit/commits/a2d5030))


null refactor media viewer adapters to inject media viewer constructor instead of us ([7b578a8](https://bitbucket.org/atlassian/atlaskit/commits/7b578a8))


* feature; integrate media collection viewer with artifact mapper ([27e2580](https://bitbucket.org/atlassian/atlaskit/commits/27e2580))


* breaking; MediaViewer adapter API changes: now requires MediaViewerConstructor.

## 2.2.0 (2017-04-07)

## 2.1.0 (2017-04-06)


* fix; fix media file attributes download url ([6012fc3](https://bitbucket.org/atlassian/atlaskit/commits/6012fc3))


* feature; add id to media file model ([b606427](https://bitbucket.org/atlassian/atlaskit/commits/b606427))
* feature; add media viewer artifact format media item mapping ([adad23b](https://bitbucket.org/atlassian/atlaskit/commits/adad23b))
* feature; add media viewer file artifact mapping ([104abe1](https://bitbucket.org/atlassian/atlaskit/commits/104abe1))
* feature; use file attribute mapper inside MediaFileViewer ([5a0e3cd](https://bitbucket.org/atlassian/atlaskit/commits/5a0e3cd))

## 1.0.0 (2017-04-04)


* feature; add media-viewer adapters ([5aee637](https://bitbucket.org/atlassian/atlaskit/commits/5aee637))
