# @atlaskit/pubsub

## 2.0.5
- [patch]  [f87724e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f87724e)

## 2.0.4
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/field-text@5.0.3
  - @atlaskit/button@7.2.5
  - @atlaskit/theme@3.2.2
  - @atlaskit/docs@3.0.4
  - @atlaskit/util-service-support@2.0.7

## 2.0.3

## 2.0.2
- [patch] FS-1948 Stop pubsub client from flooding console with 403 [bb3d588](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bb3d588)

## 2.0.1

## 2.0.0
- [major] Update to React 16.3. [8a96fc8](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8a96fc8)

## 1.1.0
- [minor] FS-1874 Move @atlassian/pubsub to @atlaskit/pubsub [92af7f7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/92af7f7)
