# @atlaskit/reactions

## 12.0.10
- [patch]  [f87724e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f87724e)
- [none] Updated dependencies [f87724e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f87724e)

## 12.0.9
- [patch] FS-1959 use @atlaskit/tooltip for reactions tooltip [ec12b0e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ec12b0e)

## 12.0.8
- [patch] FS-1975 place picker after reactions [4a6e1c5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4a6e1c5)

## 12.0.7




- [none] Updated dependencies [febc44d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/febc44d)
  - @atlaskit/util-data-test@9.1.4
  - @atlaskit/emoji@35.0.0
- [none] Updated dependencies [714ab32](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/714ab32)
  - @atlaskit/util-data-test@9.1.4
  - @atlaskit/emoji@35.0.0
- [patch] Updated dependencies [84f6f91](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/84f6f91)
  - @atlaskit/emoji@35.0.0
  - @atlaskit/util-data-test@9.1.4
- [patch] Updated dependencies [9041d71](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9041d71)
  - @atlaskit/emoji@35.0.0
  - @atlaskit/util-data-test@9.1.4

## 12.0.6
- [patch] FS-1921 Don't refresh reactions when getting detailed response if requests [1764815](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1764815)

## 12.0.5






- [none] Updated dependencies [8fd4dd1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8fd4dd1)
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/emoji@34.2.0
- [none] Updated dependencies [74f84c6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/74f84c6)
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/emoji@34.2.0
- [none] Updated dependencies [92cdf83](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/92cdf83)
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/emoji@34.2.0
- [none] Updated dependencies [4151cc5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4151cc5)
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/emoji@34.2.0
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/emoji@34.2.0
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/icon@11.3.0
  - @atlaskit/tooltip@9.2.1
  - @atlaskit/button@7.2.5
  - @atlaskit/theme@3.2.2
  - @atlaskit/docs@3.0.4
  - @atlaskit/layer@3.1.1
- [patch] Updated dependencies [89146bf](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/89146bf)
  - @atlaskit/emoji@34.2.0
  - @atlaskit/util-data-test@9.1.3

## 12.0.4
- [patch] FS-1890 Remove sinon dependency from reactions [b6ee84e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b6ee84e)
- [patch] FS-1890 Migrate Reactions to Jest [8e0e31e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8e0e31e)

## 12.0.3

## 12.0.2

## 12.0.1

## 12.0.0
- [major] FS-1023 Error handling for reactions [2202edc](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/2202edc)
- [minor] FS-1023 - Handle error and show tooltip in case of error [f9f1040](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f9f1040)

## 11.0.9
- [patch] FS-1645 update reaction animations [c01d36d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c01d36d)

## 11.0.8

## 11.0.7
- [patch] FS-1882 update reaction button to match mobile [acc8118](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/acc8118)

## 11.0.6
- [patch] FS-1876 update default reactions emoji [114cee6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/114cee6)

## 11.0.5

## 11.0.4
- [patch] FS-1644 reactions ui changes [70ccf94](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/70ccf94)

## 11.0.3
- [patch] FS-1868 always add new reactions to the end of the list [70fdbeb](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/70fdbeb)

## 11.0.2

## 11.0.1
- [patch] Added missing dependencies and added lint rule to catch them all [0672503](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0672503)

## 11.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 10.8.15
- [patch] Remove import from es6 promise at src level [e27f2ac](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e27f2ac)

## 10.8.14

## 10.8.13

## 10.8.12

## 10.8.11

## 10.8.10

## 10.8.9

## 10.8.8

## 10.8.7

## 10.8.6

## 10.8.5

## 10.8.4

## 10.8.3

## 10.8.2

## 10.8.1

## 10.8.0
- [minor] Moved Reactions to MK2 repo [d0cecbf](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d0cecbf)

## 10.7.1 (2017-12-14)

* bug fix; fix TS errors in reactions ([21c92b6](https://bitbucket.org/atlassian/atlaskit/commits/21c92b6))
## 10.7.0 (2017-11-28)

* bug fix; fS-1521 Remove util-data-test ([eb88f40](https://bitbucket.org/atlassian/atlaskit/commits/eb88f40))
* feature; fS-1521 Compatibility with react 16 ([4bd5c13](https://bitbucket.org/atlassian/atlaskit/commits/4bd5c13))
* bug fix; fS-1521 Bring back border radius ([f73ae4a](https://bitbucket.org/atlassian/atlaskit/commits/f73ae4a))
* bug fix; aK-3962 Remove react-transition-group from reactions ([da2b92d](https://bitbucket.org/atlassian/atlaskit/commits/da2b92d))
* feature; fS-994 Change reactions tooltips to be the chunky black ADG3 tooltips ([b574b07](https://bitbucket.org/atlassian/atlaskit/commits/b574b07))
* bug fix; fS-1521 Upgrade reaction component's dependencies to latest versions ([cae82c6](https://bitbucket.org/atlassian/atlaskit/commits/cae82c6))
* bug fix; fS-1521 Update emoji dependency ([405ab1a](https://bitbucket.org/atlassian/atlaskit/commits/405ab1a))







## 10.6.3 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 10.6.2 (2017-06-01)


* fix; fS-1015 Reactions randomly reorder themselves + other bugfixes ([f50a254](https://bitbucket.org/atlassian/atlaskit/commits/f50a254))

## 10.6.1 (2017-05-30)


* fix; fS-957 Make reactions wrap to newline ([2ad5cfd](https://bitbucket.org/atlassian/atlaskit/commits/2ad5cfd))

## 10.6.0 (2017-05-26)


* fix; fS-992 Emoji picker is too big for issue detail modal ([fb9ca62](https://bitbucket.org/atlassian/atlaskit/commits/fb9ca62))
* fix; fS-992 Remove popup component ([6aa5ee2](https://bitbucket.org/atlassian/atlaskit/commits/6aa5ee2))


* feature; fS-985 expose analyticService ([fbe4f67](https://bitbucket.org/atlassian/atlaskit/commits/fbe4f67))

## 10.5.1 (2017-05-22)


* fix; fS-963 Bump emoji version and fix css ([d168d44](https://bitbucket.org/atlassian/atlaskit/commits/d168d44))

## 10.3.0 (2017-05-19)

## 10.3.0 (2017-05-19)

## 10.3.0 (2017-05-19)


* fix; fS-963 Fix css ([f9f634a](https://bitbucket.org/atlassian/atlaskit/commits/f9f634a))
* fix; fS-963 Fix optimistic add/delete ([9ebe60b](https://bitbucket.org/atlassian/atlaskit/commits/9ebe60b))
* fix; fS-963 Fix PR feedbacks ([88199b4](https://bitbucket.org/atlassian/atlaskit/commits/88199b4))
* fix; fS-963 Fix reactions reappearing after deletion because of detailledreaction call ([85776d8](https://bitbucket.org/atlassian/atlaskit/commits/85776d8))
* fix; fS-963 fix test ([13232c4](https://bitbucket.org/atlassian/atlaskit/commits/13232c4))
* fix; fS-963 fix test & bump emoji ([443d8ff](https://bitbucket.org/atlassian/atlaskit/commits/443d8ff))
* fix; fS-963 Fix tests ([f10c4c6](https://bitbucket.org/atlassian/atlaskit/commits/f10c4c6))


* feature; fS-965 Update reactions design ([0451638](https://bitbucket.org/atlassian/atlaskit/commits/0451638))

## 10.2.2 (2017-05-11)


* fix; add containerAri where needed to match service contract ([bb2adca](https://bitbucket.org/atlassian/atlaskit/commits/bb2adca))
* fix; fix typescript error ([81249fb](https://bitbucket.org/atlassian/atlaskit/commits/81249fb))

## 10.2.1 (2017-05-03)


* fix; harden code to run in NodeJS environment. ([cc78477](https://bitbucket.org/atlassian/atlaskit/commits/cc78477))

## 10.2.0 (2017-05-02)


* fix; fixes a bug where long names where not being truncated ([e1ec953](https://bitbucket.org/atlassian/atlaskit/commits/e1ec953))


* feature; adding support to optionally set the text of the trigger-button ([6b5dc09](https://bitbucket.org/atlassian/atlaskit/commits/6b5dc09))

## 10.1.0 (2017-05-01)


* feature; adding support for detailed reactions ([81c6873](https://bitbucket.org/atlassian/atlaskit/commits/81c6873))
* feature; fS-767 Add analytics to reaction component ([1b5208f](https://bitbucket.org/atlassian/atlaskit/commits/1b5208f))

## 10.0.0 (2017-04-28)


* feature; adds the ablity to enable/disable all emojis ([ccacd6f](https://bitbucket.org/atlassian/atlaskit/commits/ccacd6f))


* breaking; By default the reaction picker will only allow a predefined set of emojis.

## 9.0.2 (2017-04-27)


* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 9.0.1 (2017-04-26)

## 9.0.0 (2017-04-26)


* fix; fixes emoji-size in reactions and using string rather than emojiid ([87a6af9](https://bitbucket.org/atlassian/atlaskit/commits/87a6af9))
* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))


* feature; adding containerAri ([e88190a](https://bitbucket.org/atlassian/atlaskit/commits/e88190a))


* breaking; containerAri is now a required prop for ResourcedReactionPicker and ResourcedReactions. It's also

required as first argument in toggleReaction, addReaction and deleteReaction

## 5.0.0 (2017-04-19)

## 5.0.0 (2017-04-19)

## 5.0.0 (2017-04-19)

## 5.0.0 (2017-04-19)


* feature; upgrade to latest emoji component and emoji id spec ([ae59982](https://bitbucket.org/atlassian/atlaskit/commits/ae59982))


* breaking; Emoji representation has changes due to upgrading of emoji component.

ISSUES CLOSED: FS-887

## 4.0.1 (2017-03-21)

## 4.0.1 (2017-03-21)


* fix; maintainers for all the packages were added ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))

## 4.0.0 (2017-03-09)


* feature; bump emoji dependency to ensure getting all needed exports. ([62f2487](https://bitbucket.org/atlassian/atlaskit/commits/62f2487))
* feature; upgrade to latest asynchronous Emoji ([78ce481](https://bitbucket.org/atlassian/atlaskit/commits/78ce481))


* breaking; Latest Emoji upgrade and some events are breaking changes.

## 3.0.0 (2017-03-09)


* feature; adding resourced components that takes a ReactionProvivder-promise ([b503bd9](https://bitbucket.org/atlassian/atlaskit/commits/b503bd9))


* breaking; Renamed ReactionsService to ReactionsResource, The Reactions-component now takes a

"reactionsProvider" instead of "reactionsService"

## 2.0.1 (2017-02-27)


* empty commit to make components release themselves ([5511fbe](https://bitbucket.org/atlassian/atlaskit/commits/5511fbe))

## 1.0.0 (2017-02-17)


* Fix the build and the readme story ([1915b49](https://bitbucket.org/atlassian/atlaskit/commits/1915b49))
* Fix type error in reactions-service ([09080e3](https://bitbucket.org/atlassian/atlaskit/commits/09080e3))
* Trying to fix failing CI ([2fc74cc](https://bitbucket.org/atlassian/atlaskit/commits/2fc74cc))


* Added autopoll support and logic for ignorning outdated updates ([bc7724f](https://bitbucket.org/atlassian/atlaskit/commits/bc7724f))
* Reactions Picker ([70e015a](https://bitbucket.org/atlassian/atlaskit/commits/70e015a))
