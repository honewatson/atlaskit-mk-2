# @atlaskit/analytics-listeners

## 1.1.0

- [minor] Add listener for events fired by core atlaskit components [bcc7d8f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bcc7d8f)
- [patch] Updated dependencies [9be1db0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9be1db0)
  - @atlaskit/analytics-gas-types@2.1.0

## 1.0.2
- [patch] Moved event tag to FabricElementsListener [639ae5e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/639ae5e)

## 1.0.1
- [patch] code improvements [44e55aa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/44e55aa)
- [patch] added analytics-listeners package  [8e71e9a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8e71e9a)
