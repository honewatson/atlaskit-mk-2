export type AnalyticsWebClient = {
  sendUIEvent: (event: any) => void;
  sendOperationalEvent: (event: any) => void;
  sendTrackEvent: (event: any) => void;
  sendScreenEvent: (event: any) => void;
};

export type ListenerProps = {
  children?: React.ReactNode;
  client: AnalyticsWebClient;
};

export type EventNextType = {
  payload: {
    action: string;
    [key: string]: any;
  };
  context: Array<{}>;
};

export type ListenerFunction = (event: EventNextType) => void;
