# @atlaskit/analytics-gas-types

## 2.1.0
- [minor] Export individual event types as constants [9be1db0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9be1db0)

## 2.0.0
- [major] added new package analytics-gas-types [4d238a6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4d238a6)
